grammar SimpleLists;

options {
  language = Java;
  backtrack = true;
  output = template;
}

tokens {
    INDENT;
    DEDENT;
}

@header {
    package by.bsuir.yapis.language.main;
    import by.bsuir.yapis.language.main.*;
}

@lexer::header {

    package by.bsuir.yapis.language.main; 
    import by.bsuir.yapis.language.main.*;
    import java.util.Deque;
    import java.util.LinkedList;
    import java.util.ArrayList;
}

@lexer::members {
    int level = 0;
    Stack<Integer> indents = new Stack<Integer>();
    ClassicToken token = null;
         
    Deque<Token> tokens = new java.util.ArrayDeque<Token>();
 
    @Override
    public void emit(Token token) {
        state.token = token;
        tokens.addLast(token);
    }
 
    @Override
    public Token nextToken() {
        super.nextToken();
  
        if (tokens.isEmpty())
            return Token.EOF_TOKEN;
        return tokens.removeFirst();
    }
}

@parser::members
{
    public List<Error> errors = new ArrayList<Error>();
}

program  
scope
{
    List funcTempl;
    Functions functions;
    Functions initCoroutines;
    Blocks blocks;
}
@init
{
    $program::blocks = new Blocks();
    $program::functions = new Functions();
    $program::initCoroutines = new Functions();
    $program::funcTempl = new ArrayList();
}
    :  functions NEWLINE* main EOF 
  
    ->program(functions = {$program::funcTempl}, main = {$main.st})
    ;

functions
    :  (NEWLINE* function     
       {
          $program::funcTempl.add($function.st);
       }
       )*
    ;
    
function
scope
{
    Function func;
    List paramTempl;
    FunReturn returnType;
    boolean isCor;
}
@init
{
    $function::func = new Function();
    $function::paramTempl = new ArrayList();
    $function::returnType = FunReturn.NONE;
    $function::isCor = false;
    
}
    : 'def' ID 
    {
        if($program::functions.contains($ID.text)) {
            errors.add(new Error("line " + $ID.line + " - multiple function declaration: " + $ID.text));
        }
        else {
            $function::func.setId($ID.text);
            $program::functions.add($function::func);
        }
        $function::func.setReturnValue(FunReturn.RETURN);
      }
    '(' params ')' block[$function::func.getParams()]
    {
        $function::func.setReturnValue($function::returnType);
    }
      ->function(id = {$ID.text}, params = {$function::paramTempl}, block = {$block.st}, isCor = {$function::isCor})      
    ;
    
params  : ( par = param 
          { 
              $function::func.addParam($par.text);
              $function::paramTempl.add($par.text);
          }
          
          ( comma = ',' par_2 = param 
          {
              if ($function::func.getParams().contains($par_2.text)) {
                  errors.add(new Error("line " + $comma.line + " - duplicate parameter: " + $par_2.text));
              }
              else {
                  $function::func.addParam($par_2.text);
                  $function::paramTempl.add($par_2.text);
              }
          }
          )* )? 
    ;
    
param   : ID
    ;

declarator
    : ID
    ;

main 
@init
{
    $program::blocks.addBlock(new Block());
}
@after
{
    $program::blocks.pop();
}    
    : 'main' '(' params ')' block[new ArrayList<String>()] EOF
    ->main(block = {$block.st})      
    ;
    
stmt    : stmt_assign {$block::stmtsTempl.add($stmt_assign.st);}|  
          stmt_func {$block::stmtsTempl.add($stmt_func.st);} | 
          stmt_yield {$block::stmtsTempl.add($stmt_yield.st);}| 
          stmt_return {$block::stmtsTempl.add($stmt_return.st);}| 
          stmt_break {$block::stmtsTempl.add($stmt_break.st);}| 
          stmt_if {$block::stmtsTempl.add($stmt_if.st);} | 
          stmt_print {$block::stmtsTempl.add($stmt_print.st);} | 
          stmt_while {$block::stmtsTempl.add($stmt_while.st);} | 
          stmt_for {$block::stmtsTempl.add($stmt_for.st);}| 
          stmt_for_list {$block::stmtsTempl.add($stmt_for_list.st);} | 
          stmt_get {$block::stmtsTempl.add($stmt_get.st);} |
          stmt_switch {$block::stmtsTempl.add($stmt_switch.st);}|
          stmt_coroutine {$block::stmtsTempl.add($stmt_coroutine.st);}|
          stmt_next {$block::stmtsTempl.add($stmt_next.st);} 
    ;   
    
stmt_switch
scope
{
    List caseTempls;
}
@init 
{
    $stmt_switch::caseTempls = new ArrayList();
}
    : 'switch' '(' expr ')' INDENT (case_stmt {$stmt_switch::caseTempls.add($case_stmt.st);} stmt_break? {$stmt_switch::caseTempls.add($stmt_break.st);})+ DEDENT
    -> stmt_switch(cases = {$stmt_switch::caseTempls}, expr ={$expr.st})
    ;

case_stmt: 'case' INT block[new ArrayList<String>()]
    -> case_stmt(num = {$INT.text}, block ={$block.st})
    ;    
    
stmt_get
@init
{
    boolean decl = true;
}
    : 'get' ID
    {
    if($program::blocks.checkIdInBlocks($ID.text) || $program::blocks.checkIdForVars($ID.text))
            decl = false;
        else $program::blocks.addVarToTopBlock($ID.text);
    }
    ->stmt_get(ID = {$ID.text}, decl = {decl})
    ;    
 
stmt_print
    : 'print' expr
    ->stmt_print(expr = {$expr.st})
    ;    

stmt_assign
@init
{
    boolean decl = true;
    String var = new String();
}
    : (id = ID 
    {
        if($program::blocks.checkIdInBlocks($id.text) || $program::blocks.checkIdForVars($id.text))
            decl = false;
        else $program::blocks.addVarToTopBlock($id.text);
        var = $id.text;    
    }
    | listId = ID '[' expr ']' 
    {
        decl = false;
        var = $listId.text + ".get(" + $expr.st.toString() + ")";
    }
    ) ASSIGN value
    ->stmt_assign(id = {var}, expr = {$value.st}, decl = {decl})
    ;
 
 value
    : expr 
      ->{$expr.st} | 
      list
      ->{$list.st}
    ;

list
@init
{
    List exprs = new ArrayList();
}
    :('[' ((expr_1 = expr
    {
        exprs.add($expr_1.st);
    }
    ) ( ',' (expr_2 = expr
    {
        exprs.add($expr_2.st);
    }
    ) )* )? ']')
    ->list(exprs = {exprs})
    ;    

stmt_next
    : ID '.next'
    {
        Function func = null;
        for(Function f: $program::functions.getFunctions())
            if (f.getId().equals($ID.text))
                func = f;
        if (func == null) 
            errors.add(new Error("line " + $ID.line + " - invalid coroutine identifier: " + $ID.text)); 
        else if (func.getReturnValue() != FunReturn.YIELD)       
            errors.add(new Error("line " + $ID.line + " - not a coroutine identifier: " + $ID.text)); 
        else if (!$program::initCoroutines.contains(func.getId()))   
            errors.add(new Error("line " + $ID.line + " - coroutine hasn't been started: " + $ID.text));    
    }
    ->stmt_next()
    ; 

stmt_coroutine
    : ID '.begin' '(' call_params ')' 
    {
        Function func = null;
        for(Function f: $program::functions.getFunctions())
            if (f.getId().equals($ID.text))
                func = f;
        if (func == null) 
            errors.add(new Error("line " + $ID.line + " - invalid coroutine identifier: " + $ID.text)); 
        else if (func.getReturnValue() != FunReturn.YIELD)       
            errors.add(new Error("line " + $ID.line + " - not a coroutine identifier: " + $ID.text)); 
        else if (!$program::initCoroutines.contains(func.getId()))   
                $program::initCoroutines.add(func);            
    }
    ->stmt_coroutine(id = {$ID.text}, callParams = {$call_params.callParams})
    ; 

stmt_func returns[Function func, String id]
scope
{
    boolean isCor;
}
@init
{
    
}
    : ID '(' call_params ')'
    {
        $func = null;
        for(Function f: $program::functions.getFunctions())
            if (f.getId().equals($ID.text))
                $func = f;
        $id = $ID.text;
        int paramsNum;
        if($call_params.callParams == null)
            paramsNum = 0;
        else paramsNum = $call_params.callParams.size();
        
        
        if($stmt_func.func == null)
            errors.add(new Error("line " + $ID.line + " - invalid function identifier: " + $stmt_func.id));            
        else if(paramsNum != $func.getParams().size())
            errors.add(new Error("line " + $ID.line + " - invalid params number: " + $func.getId()));
        
        if ($func.getReturnValue() == FunReturn.YIELD)    
            $stmt_func::isCor = true;   
        else $stmt_func::isCor = false;   
    }
    ->stmt_func(id = {$ID.text}, callParams = {$call_params.callParams}, isCor = {$stmt_func::isCor})
    ;
    
stmt_if
    : 'if' '(' log_stmt ')' block[new ArrayList<String>()] stmt_else?
    ->stmt_if(logStmt = {$log_stmt.st}, block = {$block.st}, elseStmt = {$stmt_else.st})
    ;    

stmt_else: 'else' block[new ArrayList<String>()]
    ->stmt_else(block = {$block.st})
    ;

stmt_while
    : 'while' '(' log_stmt ')' block[new ArrayList<String>()]
    ->stmt_while(logStmt = {$log_stmt.st}, block = {$block.st})
    ;

stmt_for
scope 
{
    List<String> assigned;
}
@init
{
    $stmt_for::assigned = new ArrayList<String>();
}
@after
{    
    $program::blocks.delForVar();;
}
    : 'for' '(' for_stmts ')' block[$stmt_for::assigned]
    ->stmt_for(forStmts = {$for_stmts.st}, block = {$block.st})
    ;

for_stmts 
    : assign_1 = for_assign[true]? SEMI log_stmt? SEMI assign_2 = for_assign[false]?
    ->for_stmts(stmtAssign = {$assign_1.st}, logStmt = {$log_stmt.st}, stmtAssign2 = {$assign_2.st})
    ;    

for_assign[boolean isFirst]
    : (id = ID 
    {
        if(isFirst) {
            if ($program::blocks.checkIdInBlocks($id.text) || $program::blocks.checkIdForVars($id.text)) 
                isFirst = false;   
            
            $program::blocks.addForVar($id.text);
        }
         
    }
    ) ASSIGN value
    ->for_assign(id = {$id.text}, expr = {$value.st}, isFirst = {$isFirst})
    ;
    
stmt_for_list
    : 'foreach' '(' ex_1 = ID {$program::blocks.addForVar($ex_1.text);} 'in' ex_2 = expr ')' block[new ArrayList()]
    ->stmt_for_list(ex_1 = {$ex_1.text}, ex_2 = {$ex_2.st}, block = {$block.st})
    ;
    
stmt_yield
    : 'yield' expr 
    {
        $function::returnType = FunReturn.YIELD;
        $function::isCor = true;
    }
    ->stmt_yield(expr = {$expr.st})
    ;    
    
stmt_return
    : 'return' expr 
    {
        $function::returnType = FunReturn.RETURN;
    }
    ->stmt_return(expr = {$expr.st})
    ;
        
stmt_break
    : 'break'
    ->stmt_break()
    ;
    
call_params returns[List<String> callParams]
    : ( paramsLabel+=expr ( ',' paramsLabel+=expr )* )? {$callParams = $paramsLabel;}
    ;
    
stmts
    : (NEWLINE* stmt)*
    ;	  

log_stmt
@init
{
    String logOp = new String();
}
    : expr_1 = expr ((LOGOP expr_2 = expr)
    {
        if($LOGOP.text.equals("=="))
            logOp = "equals";
        else if($LOGOP.text.equals("<"))
            logOp = "less";   
        else if($LOGOP.text.equals(">"))
            logOp = "greater";  
        else if($LOGOP.text.equals("!="))
            logOp = "notEquals";           
    } 
    -> log_stmt(expr_1 = {$expr_1.st}, expr_2 = {$expr_2.st}, logOp = {logOp}) | 
    (is_expr[$expr_1.st])
    -> {$is_expr.st}
    )
    ;    	  
    
is_expr[StringTemplate expr]    
	: '.' (ISINT 
	-> isInt(expr = {$expr}) | 
	ISSTRING
	-> isString(expr = {$expr}) |
	ISLIST
	-> isList(expr = {$expr})
	)	
	;	
 
expr
@init
{
    List multExprs = new ArrayList();
}
    :	mult = mult_expr (('+' {multExprs.add(".add(");}|'-'{multExprs.add(".sub(");}) mult_2 = mult_expr
    {
        multExprs.add($mult_2.st.toString());
        multExprs.add(")");
    }
    )*
    ->expr(startExpr = {$mult.st}, multExprs = {multExprs})
    ;   	 
 
mult_expr
@init
{
    List atoms = new ArrayList();
}
    : atom_1 = atom 
    (('*' {atoms.add(".mult(");}|'/' {atoms.add(".div(");}) atom_2 = atom
    {
        atoms.add($atom_2.st.toString());
        atoms.add(")");
    }
    )*
    ->mult_expr(startAtom = {$atom_1.st}, atoms = {atoms})
    ;
    
atom	
@init
{
    String forTemplate = new String();
}
    : (ID 
    {
        if(!$program::blocks.checkIdInBlocks($ID.text) && !$program::blocks.checkIdForVars($ID.text))
            errors.add(new Error("line " + $ID.line + " - undefined variable in expression: " + $ID.text));
        forTemplate = $ID.text;    
    } | 
    stmt_func 
    {
        if ($stmt_func.func != null && $stmt_func.func.getReturnValue() == FunReturn.NONE)
            errors.add(new Error("line " + $ID.line + " - function used in the expression does not return value: " 
            + $stmt_func.func.getId()));
        forTemplate = $stmt_func.st.toString();    
    } | 
    element 
    {
        forTemplate = "(new Type(" + $element.text + "))";
    } | 
    list 
    {
        forTemplate = $list.st.toString();
    } |
    '(' expr ')' 
    {
        forTemplate = '(' + $expr.st.toString() + ')';
    } |
    stmt_next 
    {
        forTemplate = $stmt_next.st.toString();
    }
    ) 
    index? 
    point_expr?
    ->atom(text = {forTemplate}, index = {$index.st}, point_expr = {$point_expr.st})
    ;    

point_expr
    : ('.' (TOINT ->to_int() | 
            TOSTRING ->to_string()| 
            LENGTH ->length() |
            list_ops ->{$list_ops.st}
      ))
    ;

list_ops
    : DEL '(' expr ')' ->del(expr = {$expr.st}) | ADD '(' expr ')' ->add(expr = {$expr.st})
    ;

index
    : ('[' expr ']')
    ->index(expr = {$expr.st})
    ;   	    
   	       
block[List<String> params]
scope
{
    List stmtsTempl;
}
@init
{
    $block::stmtsTempl = new ArrayList();
    Block block = new Block();
    for(String par: params)
        block.addVar(par);
    $program::blocks.addBlock(block);
        
}
@after
{
    $program::blocks.pop();
}  
  :  
   INDENT stmts NEWLINE* DEDENT 
   ->block(stmts = {$block::stmtsTempl})
  ;    
      
element 
    : INT | STRING    
    ;        
       
LOGOP   : EQUAL | LESS | GREATER | NOTEQUAL | LESSEQUAL | GREATEREQUAL;        
       
COLON   : ':' ;

COMMA   : ',' ;

SEMI    : ';' ;

PLUS    : '+' ;

MINUS   : '-' ;

STAR    : '*' ;

SLASH   : '/' ;

ISINT 	: 'int?' ;

ISSTRING
 	: 'string?' ;
 	
ISLIST  : 'list?' ; 	
 	
TOINT	: 'int!';

TOSTRING: 'string!' ;	 	

LENGTH: 'length' ;

ADD: 'add';

DEL: 'del';

fragment
LESS    : '<' ;

fragment
GREATER : '>' ;

ASSIGN  : '=' ;

fragment
EQUAL   : '==' ;

fragment
NOTEQUAL    : '!=' ;

fragment
LESSEQUAL   : '<=' ;

fragment
GREATEREQUAL    : '>=' ;

DOUBLESLASH : '//' ;

DOT : '.' ;

ID  :   ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

CONTINUED_LINE
    : '\\' ('\r')? '\n' (' '|'\t')* { $channel=HIDDEN; }
    ;


fragment
INDENTATION 
@init { 
    level = 0;
}
    
    : 
    { getCharPositionInLine()==0 }?=>
    (NEWLINE | (' ')* COMMENT {skip();} |
    ((' '  { level++; } )*)
    {  
        if(indents.empty()) 
            indents.push(0);

        if (level > indents.peek().intValue())
        {
            emit(new ClassicToken(INDENT,"INDENT"));
            indents.push(level);
        } 
  
        else if(level < indents.peek().intValue())
        {
            while (level < indents.peek().intValue()) 
            {
                indents.pop();
                emit(new ClassicToken(DEDENT,"DEDENT"));
             }
        }
        else if(level == indents.peek().intValue()) 
            skip();
    }
    )
    ;

WS  : (' '|'\t')+ {$channel=HIDDEN;}
    ;
    
NEWLINE
    :   (('\r')? '\n' ) (INDENTATION | EOF)
    ;


COMMENT : '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    ;

INT
    :   DIGIT+ 
    ;

STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;

fragment
HEX_DIGIT : (DIGIT|'a'..'f'|'A'..'F') ;

fragment
DIGIT   : ('0'..'9') ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;
