package by.bsuir.yapis.language.main;

import java.util.ArrayList;
import java.util.List;

public class Block {	
	private List<String> variables;
	
	public Block() {
		setVariables(new ArrayList<String>());
	}

	public boolean checkId(String id) {
		for(String var: getVariables()) 
			if (var.equals(id))
				return true;
		return false;
	}
	
	public void addVar(String var) {
		getVariables().add(var);	
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}
}
