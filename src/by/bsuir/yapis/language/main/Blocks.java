package by.bsuir.yapis.language.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Blocks {

	private Stack<Block> blocks;
	private List<String> forVars;
	
	public Blocks() {
		blocks = new Stack<Block>();
		forVars = new ArrayList<String>();
	}
	
	public void addBlock(Block block) {
		blocks.push(block);
	}
	
	public void addForVar(String var) {
		forVars.add(var);
	}
	
	public void delForVar() {
		forVars.remove(forVars.size()-1);
	}
	
	public boolean checkIdForVars(String id) {
		return forVars.contains(id);
	}
	
	public Block getTop() {
		return blocks.get(blocks.size()-1);
	}
	
	public void pop() {
		blocks.pop();
	}
	
	public boolean checkIdInTop(String id) {
		Block top = getTop();
		System.out.println(id); 

		for(Block block: blocks)
			System.out.println(block.getVariables()); 
		System.out.println("==============================="); 
		return top.checkId(id);			
	}
	
	public boolean checkIdInBlocks(String id) {
		for(Block block: blocks)
			if (block.checkId(id) == true)
				return true;  
		return false;			
	}
	
	public void addVarToTopBlock(String var) {
		blocks.get(blocks.size()-1).addVar(var);
	}

	public Stack<Block> getBlocks() {
		return blocks;
	}

	public void setBlocks(Stack<Block> blocks) {
		this.blocks = blocks;
	}
}

