package by.bsuir.yapis.language.main;

public class Element extends Type {

	private ElType type;	
	private String value;	
	
	public Element(String value, ElType type) {
		super();
		this.value = value;
		this.type = type;
	}

	public Element() {
		super();
	}
	
	public Element(Element copy) {
		super();
		this.value = copy.value;
		this.type = copy.type;
	}

	public ElType getType() {
		return type;
	}

	public void setType(ElType type) {
		this.type = type;
	}
	public Type changeType(ElType type) {
		Element retEl = new Element();
		retEl.setValue(this.value);
		retEl.setType(type);
		Type retType = new Type();
		retType.setVarType(retEl);
		return retType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public void set(Element assignEl) {
		this.value = assignEl.getValue();
	}
	
	public void set(int value) {
		this.value = String.valueOf(value);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Element))
			return false;
		
		Element comp = (Element)obj;
		if (type != comp.getType())
			return false;
		if (!value.equals(comp.getValue()))
			return false;
		return true;
	}
	
	public boolean less(Type compType){
		if(compType.getClass().getSimpleName().equals("SimpleList"))
			return true;
		Element comp = (Element)compType;
		if(type == ElType.INTEGER) {
			if(comp.getType() == ElType.STRING)
				return true;
			else return Integer.parseInt(value) < Integer.parseInt(comp.getValue());
		}
		if(type == ElType.STRING) {
			if(comp.getType() == ElType.INTEGER)
				return false;
			else if (value.compareTo(comp.getValue()) < 0)
				return true;
			else return false;
		}
		return false;
	}
	@Override
	public boolean greater(Type compType){
		if(compType.getClass().getSimpleName().equals("SimpleList"))
			return false;
		Element comp = (Element)compType;
		if(type == ElType.INTEGER) {
			if(comp.getType() == ElType.STRING)
				return false;
			else return Integer.parseInt(value) > Integer.parseInt(comp.getValue());
		}
		if(type == ElType.STRING) {
			if(comp.getType() == ElType.INTEGER)
				return true;
			else if (value.compareTo(comp.getValue()) > 0)
				return true;
			else return false;
		}
		return false;
	}
	
	public Type add(int i) {
		int elVal = Integer.parseInt(getValue());
		Element resEl = new Element(String.valueOf(elVal + i), ElType.INTEGER);
		Type resType = new Type();
		resType.setVarType(resEl);
		return resType;
	}	
	
	public Type sub(int i) {
		int elVal = Integer.parseInt(getValue());
		Element resEl = new Element(String.valueOf(elVal - i), ElType.INTEGER);
		Type resType = new Type();
		resType.setVarType(resEl);
		return resType;
	}	
	
	public Type add(SimpleList toAdd) {
		return toAdd.add(this);
	}	
	
	public Type sub(SimpleList toSub) {
		return new Type(value);
	}
	
	public Type add(Element toAdd) {
		Type resType = new Type();
		if (toAdd.getType() == ElType.STRING || type == ElType.STRING) {
			Element resEl = new Element(value + toAdd.getValue(), ElType.STRING);
			resType.setVarType(resEl);
			return resType;			
		}
		int elVal = Integer.parseInt(getValue());
		int toAddVal = Integer.parseInt(toAdd.getValue());
		Element resEl = new Element(String.valueOf(elVal + toAddVal), ElType.INTEGER);
		resType.setVarType(resEl);
		return resType;
	}
	
	public Type sub(Element toSub) {
		Type resType = new Type();
		if (toSub.getType() == ElType.STRING || type == ElType.STRING) {
			resType.setVarType(new Element(this));
			return resType;			
		}
		int elVal = Integer.parseInt(getValue());
		int toSubVal = Integer.parseInt(toSub.getValue());
		Element resEl = new Element(String.valueOf(elVal - toSubVal), ElType.INTEGER);
		resType.setVarType(resEl);
		return resType;
	}
	
	public Type mult(int mult) {
		int thisVal = Integer.parseInt(value);
		Type newType = new Type(thisVal*mult);
		return newType;
	}
	
	public Type mult(Element mult) {
		int thisVal = Integer.parseInt(value);
		int multiplr = Integer.parseInt(mult.getValue());
		Type newType = new Type(thisVal*multiplr);
		return newType;
	}
	
	public Type div(int div) {
		int thisVal = Integer.parseInt(value);
		Type newType = new Type(thisVal/div);
		return newType;
	}
	
	public Type div(Element div) {
		int thisVal = Integer.parseInt(value);
		int multiplr = Integer.parseInt(div.getValue());
		Type newType = new Type(thisVal/multiplr);
		return newType;
	}
	
	
	@Override
	public int hashCode() {
		int code = 11;
		code = code*7 + value.hashCode();
		code = code*7 + type.hashCode();
		return code;
	}
	
	@Override
	public String toString() {
		return value;
	}
}

