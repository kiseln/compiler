package by.bsuir.yapis.language.main;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import jyield.*;
import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RuleReturnScope;
import org.antlr.stringtemplate.StringTemplateGroup;
import org.antlr.stringtemplate.language.AngleBracketTemplateLexer;

public class EntryPoint {

	private static java.util.Scanner sc = new java.util.Scanner(System.in);

	public static StringTemplateGroup templates;

	public static void main(String[] args) throws Exception {

		String templateFileName = "src/template.stg";
		templates = new StringTemplateGroup(new FileReader(templateFileName),
				AngleBracketTemplateLexer.class);

		// System.out.println("input file path :");
		String path = "";

		// System.out.println("input file name: ");
		// String name = sc.nextLine();

		String fullpath = "test.txt"; // path + name;
		try {
			CharStream input = new ANTLRFileStream(fullpath);

			SimpleListsLexer lexer = new SimpleListsLexer(input);

			CommonTokenStream tokens = new CommonTokenStream(lexer);

			SimpleListsParser parser = new SimpleListsParser(tokens);
			parser.setTemplateLib(templates);

			RuleReturnScope r = parser.program();
			// System.out.println("code :  ");
			// System.out.println(r.getTemplate().toString());

			if(!parser.errors.isEmpty()) {
				for(Error err: parser.errors)
					System.out.println(err.getMessage());
				return;
			}
				PrintWriter out = new PrintWriter(new FileWriter(path
						+ "src/program.java"), true);
				out.println(r.getTemplate().toString());

			
				
				compile(path);

				File file = new File(path + "compile.bat");
				file.delete();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static void compile(String path) {
		// create bat
		PrintWriter out;
		try {
			out = new PrintWriter(new FileWriter(path + "compile.bat"), true);

			String bat =  "cd src"
					+ " \r\n javac -classpath jyield-0.0.6-jar-with-dependencies.jar; program.java \r\n  java  -javaagent:jyield-0.0.6-jar-with-dependencies.jar program";
			out.println(bat);
			Runtime runtime = Runtime.getRuntime();
			try {
				Process p1 = runtime.exec("cmd /c start " + path
						+ "compile.bat");
				InputStream is = p1.getInputStream();
				int i = 0;
				while ((i = is.read()) != -1) {
					System.out.print((char) i);
				}
			} catch (IOException ioException) {
				System.out.println(ioException.getMessage());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
