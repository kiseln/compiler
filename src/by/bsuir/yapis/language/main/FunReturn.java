package by.bsuir.yapis.language.main;

public enum FunReturn {
	RETURN,
	YIELD,
	NONE
}