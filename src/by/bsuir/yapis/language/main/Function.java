package by.bsuir.yapis.language.main;

import java.util.ArrayList;
import java.util.List;

public class Function {
	
	private String Id;
	private FunReturn returnType;
	private List<String> params;
	
	public Function() {
		params = new ArrayList<String>();
	}
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public FunReturn getReturnValue() {
		return returnType;
	}
	public void setReturnValue(FunReturn returnType) {
		this.returnType = returnType;
	}
	
	public void addParam(String param) {
		params.add(param);
	}
	public List<String> getParams() {
		return params;
	}
	
	
}
