package by.bsuir.yapis.language.main;

import java.util.ArrayList;
import java.util.List;

public class Functions {
	
	private List<Function> functions;
	
	public Functions(){
		functions = new ArrayList<Function>();
	}
	
	public boolean contains(String id){
		for(Function fun: getFunctions())
		    if(fun.getId().equals(id))
		    	return true;
		return false;
	}
	public void add(Function function){
		functions.add(function);
	}

	public List<Function> getFunctions() {
		return functions;
	}
}
