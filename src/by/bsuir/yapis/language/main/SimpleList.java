package by.bsuir.yapis.language.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SimpleList extends Type {
	
	private List<Type> elements;
	
	public SimpleList(List<Type> elements) {
		this.elements = elements;
	}
	
	public SimpleList(Iterable<Type> iter) {
		super();
		elements = new ArrayList<Type>();
		for(Type el: iter)
			elements.add(el);
	}
	
	public SimpleList(Object[] array) {
		super();
		elements = new ArrayList<Type>();
		for(Object el: array)
			if(el instanceof String) {
				elements.add(new Type(el.toString()));		
		    }
			else if (el instanceof Integer)
				elements.add(new Type(Integer.parseInt(el.toString())));	
			else if (el instanceof Object[])
				elements.add(new Type((Object[])el));
			else elements.add(new Type((Type)el));
	}
	
	public SimpleList(SimpleList copy) {
		super();
		this.elements = new ArrayList<Type>(copy.getElements());
	}
	
	public void addEl(String value, ElType type) {
		Type toAdd = new Type();
		toAdd.setVarType(new Element(value, type));
		getElements().add(toAdd);		
	}

	public Type add(Element element) {
		Type newType = new Type();
		SimpleList newList = new SimpleList(this);
		newList.addEl(element.getValue(), element.getType());
		newType.setVarType(newList);
		return newList;
	}
	
	public Type sub(Element element) {
		if (element.getType() == ElType.INTEGER)
			return sub(Integer.parseInt(element.getValue()));
		else return sub(element.getValue());
	}
	
	public Type add(int element) {
		Type newType = new Type();
		SimpleList newList = new SimpleList(this);
		newList.addEl(String.valueOf(element), ElType.INTEGER);
		newType.setVarType(newList);
		return newList;
	}
	
	public Type add(Type type) {
		Type newType = new Type();
		SimpleList newList = new SimpleList(this);
		Type toAdd = new Type(type);
		newList.getElements().add(toAdd);
		newType.setVarType(newList);
		return newType;
	}
	
	public Type sub(int element) {
		Type newType = new Type();
		SimpleList newList = new SimpleList(this);
		newList.getElements().removeAll(Arrays.asList(new Type(element)));
		newType.setVarType(newList);
		return newType;
	}
	
	public Type add(String element) {
		Type newType = new Type();
		SimpleList newList = new SimpleList(this);
		newList.addEl(element, ElType.STRING);
		newType.setVarType(newList);
		return newType;
	}
	
	public Type sub(String element) {
		Type newType = new Type();
		SimpleList newList = new SimpleList(this);
		newList.getElements().removeAll(Arrays.asList(new Type(element)));
		newType.setVarType(newList);
		return newType;
	}
	
	public Type add(SimpleList list) {
		Type newType = new Type();
		List<Type> union = new ArrayList<Type>(elements);
		for(Type t: list.getElements())
			if(!isInclude(t))
				union.add(t);
		SimpleList newList = new SimpleList(union);
		newType.setVarType(newList);
		return newType;
	}
	
	public Type sub(SimpleList list) {
		Type newType = new Type();
		List<Type> substr = new ArrayList<Type>(elements);
		for(Type t: list.getElements())
			if(isInclude(t))
				substr.removeAll(Arrays.asList(t));
		SimpleList newList = new SimpleList(substr);
		newType.setVarType(newList);
		return newType;
	}
	
	public Type mult(SimpleList list) {
		Type newType = new Type();
		List<Type> intersection = new ArrayList<Type>();
		for(Type t: list.getElements())
			if(isInclude(t))
				intersection.add(t);
		SimpleList newList = new SimpleList(intersection);
		newType.setVarType(newList);
		return newType;
	}
	
	public Type symSub(SimpleList list) {
		Type newType = new Type();
		List<Type> substr = new ArrayList<Type>(elements);
		for(Type t: list.getElements())
			if(isInclude(t))
				substr.removeAll(Arrays.asList(t));
		for(Type t: list.getElements())
			if(!isInclude(t))
				substr.add(t);
		SimpleList newList = new SimpleList(substr);
		newType.setVarType(newList);
		return newType;
	}
	
	public Type del(int index) {
		Type newType = new Type();
		SimpleList newList = new SimpleList(this);
		newList.getElements().remove(index);
	    newType.setVarType(newList);
		return newType;
	}
	
	public boolean isInclude(Type toFind) {
		for(Type t: elements)
		    if(toFind.equals(t))
		    	return true;
		return false;
	}
	
	public Type length() {
		return new Type(getElements().size());		
	}
	
	public void set(SimpleList assignEl) {
		this.elements = assignEl.getElements();
	}

	public List<Type> getElements() {
		return elements;
	}

	public void setElements(List<Type> elements) {
		this.elements = elements;
	}
	
	public boolean less(Type compType) {
			if(compType.getClass().getSimpleName().equals("Element"))
				return false;
			SimpleList comp = (SimpleList)compType;
			return elements.size() < comp.getElements().size();
	}
	
	public boolean greater(Type compType) {
		if(compType.getClass().getSimpleName().equals("Element"))
			return true;
		SimpleList comp = (SimpleList)compType;
		return elements.size() > comp.getElements().size();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof SimpleList))
			return false;
		SimpleList comp = (SimpleList)obj;
		if (elements.size() != comp.getElements().size())
			return false;
		for(int i = 0; i < elements.size(); i++)
			if (!elements.get(i).equals(comp.getElements().get(i)))
				return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		int code = 11;
		code = code*7 + elements.hashCode();
		return code;
	}
	
	@Override
	public String toString() {
		if (elements.isEmpty()) return "";
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if(elements.get(0).getType() == ElType.STRING) { 
			sb.append("\"");
			sb.append(elements.get(0).toString());
			sb.append("\"");
		}
		else sb.append(elements.get(0).toString());
		for(int i = 1; i < elements.size(); i++) {
			sb.append(",");
			if(elements.get(i).getType() == ElType.STRING) { 
				sb.append("\"");
				sb.append(elements.get(i).toString());
				sb.append("\"");
			}
			else sb.append(elements.get(i).toString());
		}
		sb.append("]");
		return sb.toString();
	}
}
