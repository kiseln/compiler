// $ANTLR 3.3 Nov 30, 2010 12:50:56 D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g 2013-06-19 01:28:00


    package by.bsuir.yapis.language.main; 
    import by.bsuir.yapis.language.main.*;
    import java.util.Deque;
    import java.util.LinkedList;
    import java.util.ArrayList;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class SimpleListsLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int INDENT=4;
    public static final int DEDENT=5;
    public static final int NEWLINE=6;
    public static final int ID=7;
    public static final int INT=8;
    public static final int ASSIGN=9;
    public static final int SEMI=10;
    public static final int LOGOP=11;
    public static final int ISINT=12;
    public static final int ISSTRING=13;
    public static final int ISLIST=14;
    public static final int TOINT=15;
    public static final int TOSTRING=16;
    public static final int LENGTH=17;
    public static final int DEL=18;
    public static final int ADD=19;
    public static final int STRING=20;
    public static final int EQUAL=21;
    public static final int LESS=22;
    public static final int GREATER=23;
    public static final int NOTEQUAL=24;
    public static final int LESSEQUAL=25;
    public static final int GREATEREQUAL=26;
    public static final int COLON=27;
    public static final int COMMA=28;
    public static final int PLUS=29;
    public static final int MINUS=30;
    public static final int STAR=31;
    public static final int SLASH=32;
    public static final int DOUBLESLASH=33;
    public static final int DOT=34;
    public static final int CONTINUED_LINE=35;
    public static final int COMMENT=36;
    public static final int INDENTATION=37;
    public static final int WS=38;
    public static final int DIGIT=39;
    public static final int ESC_SEQ=40;
    public static final int HEX_DIGIT=41;
    public static final int UNICODE_ESC=42;
    public static final int OCTAL_ESC=43;

        int level = 0;
        Stack<Integer> indents = new Stack<Integer>();
        ClassicToken token = null;
             
        Deque<Token> tokens = new java.util.ArrayDeque<Token>();
     
        @Override
        public void emit(Token token) {
            state.token = token;
            tokens.addLast(token);
        }
     
        @Override
        public Token nextToken() {
            super.nextToken();
      
            if (tokens.isEmpty())
                return Token.EOF_TOKEN;
            return tokens.removeFirst();
        }


    // delegates
    // delegators

    public SimpleListsLexer() {;} 
    public SimpleListsLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public SimpleListsLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g"; }

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:38:7: ( 'def' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:38:9: 'def'
            {
            match("def"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:39:7: ( '(' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:39:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:40:7: ( ')' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:40:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:41:7: ( 'main' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:41:9: 'main'
            {
            match("main"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:42:7: ( 'switch' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:42:9: 'switch'
            {
            match("switch"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:43:7: ( 'case' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:43:9: 'case'
            {
            match("case"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:44:7: ( 'get' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:44:9: 'get'
            {
            match("get"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:45:7: ( 'print' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:45:9: 'print'
            {
            match("print"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:46:7: ( '[' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:46:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:47:7: ( ']' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:47:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:48:7: ( '.next' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:48:9: '.next'
            {
            match(".next"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:49:7: ( '.begin' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:49:9: '.begin'
            {
            match(".begin"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:50:7: ( 'if' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:50:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "T__57"
    public final void mT__57() throws RecognitionException {
        try {
            int _type = T__57;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:51:7: ( 'else' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:51:9: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__57"

    // $ANTLR start "T__58"
    public final void mT__58() throws RecognitionException {
        try {
            int _type = T__58;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:52:7: ( 'while' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:52:9: 'while'
            {
            match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__58"

    // $ANTLR start "T__59"
    public final void mT__59() throws RecognitionException {
        try {
            int _type = T__59;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:53:7: ( 'for' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:53:9: 'for'
            {
            match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__59"

    // $ANTLR start "T__60"
    public final void mT__60() throws RecognitionException {
        try {
            int _type = T__60;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:54:7: ( 'foreach' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:54:9: 'foreach'
            {
            match("foreach"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__60"

    // $ANTLR start "T__61"
    public final void mT__61() throws RecognitionException {
        try {
            int _type = T__61;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:55:7: ( 'in' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:55:9: 'in'
            {
            match("in"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__61"

    // $ANTLR start "T__62"
    public final void mT__62() throws RecognitionException {
        try {
            int _type = T__62;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:56:7: ( 'yield' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:56:9: 'yield'
            {
            match("yield"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__62"

    // $ANTLR start "T__63"
    public final void mT__63() throws RecognitionException {
        try {
            int _type = T__63;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:57:7: ( 'return' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:57:9: 'return'
            {
            match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__63"

    // $ANTLR start "T__64"
    public final void mT__64() throws RecognitionException {
        try {
            int _type = T__64;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:58:7: ( 'break' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:58:9: 'break'
            {
            match("break"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__64"

    // $ANTLR start "LOGOP"
    public final void mLOGOP() throws RecognitionException {
        try {
            int _type = LOGOP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:554:9: ( EQUAL | LESS | GREATER | NOTEQUAL | LESSEQUAL | GREATEREQUAL )
            int alt1=6;
            switch ( input.LA(1) ) {
            case '=':
                {
                alt1=1;
                }
                break;
            case '<':
                {
                int LA1_2 = input.LA(2);

                if ( (LA1_2=='=') ) {
                    alt1=5;
                }
                else {
                    alt1=2;}
                }
                break;
            case '>':
                {
                int LA1_3 = input.LA(2);

                if ( (LA1_3=='=') ) {
                    alt1=6;
                }
                else {
                    alt1=3;}
                }
                break;
            case '!':
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:554:11: EQUAL
                    {
                    mEQUAL(); 

                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:554:19: LESS
                    {
                    mLESS(); 

                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:554:26: GREATER
                    {
                    mGREATER(); 

                    }
                    break;
                case 4 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:554:36: NOTEQUAL
                    {
                    mNOTEQUAL(); 

                    }
                    break;
                case 5 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:554:47: LESSEQUAL
                    {
                    mLESSEQUAL(); 

                    }
                    break;
                case 6 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:554:59: GREATEREQUAL
                    {
                    mGREATEREQUAL(); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LOGOP"

    // $ANTLR start "COLON"
    public final void mCOLON() throws RecognitionException {
        try {
            int _type = COLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:556:9: ( ':' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:556:11: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COLON"

    // $ANTLR start "COMMA"
    public final void mCOMMA() throws RecognitionException {
        try {
            int _type = COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:558:9: ( ',' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:558:11: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMA"

    // $ANTLR start "SEMI"
    public final void mSEMI() throws RecognitionException {
        try {
            int _type = SEMI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:560:9: ( ';' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:560:11: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SEMI"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:562:9: ( '+' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:562:11: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "MINUS"
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:564:9: ( '-' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:564:11: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "STAR"
    public final void mSTAR() throws RecognitionException {
        try {
            int _type = STAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:566:9: ( '*' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:566:11: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "STAR"

    // $ANTLR start "SLASH"
    public final void mSLASH() throws RecognitionException {
        try {
            int _type = SLASH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:568:9: ( '/' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:568:11: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SLASH"

    // $ANTLR start "ISINT"
    public final void mISINT() throws RecognitionException {
        try {
            int _type = ISINT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:570:8: ( 'int?' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:570:10: 'int?'
            {
            match("int?"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ISINT"

    // $ANTLR start "ISSTRING"
    public final void mISSTRING() throws RecognitionException {
        try {
            int _type = ISSTRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:573:3: ( 'string?' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:573:5: 'string?'
            {
            match("string?"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ISSTRING"

    // $ANTLR start "ISLIST"
    public final void mISLIST() throws RecognitionException {
        try {
            int _type = ISLIST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:575:9: ( 'list?' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:575:11: 'list?'
            {
            match("list?"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ISLIST"

    // $ANTLR start "TOINT"
    public final void mTOINT() throws RecognitionException {
        try {
            int _type = TOINT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:577:7: ( 'int!' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:577:9: 'int!'
            {
            match("int!"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "TOINT"

    // $ANTLR start "TOSTRING"
    public final void mTOSTRING() throws RecognitionException {
        try {
            int _type = TOSTRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:579:9: ( 'string!' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:579:11: 'string!'
            {
            match("string!"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "TOSTRING"

    // $ANTLR start "LENGTH"
    public final void mLENGTH() throws RecognitionException {
        try {
            int _type = LENGTH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:581:7: ( 'length' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:581:9: 'length'
            {
            match("length"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LENGTH"

    // $ANTLR start "ADD"
    public final void mADD() throws RecognitionException {
        try {
            int _type = ADD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:583:4: ( 'add' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:583:6: 'add'
            {
            match("add"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ADD"

    // $ANTLR start "DEL"
    public final void mDEL() throws RecognitionException {
        try {
            int _type = DEL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:585:4: ( 'del' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:585:6: 'del'
            {
            match("del"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DEL"

    // $ANTLR start "LESS"
    public final void mLESS() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:588:9: ( '<' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:588:11: '<'
            {
            match('<'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "LESS"

    // $ANTLR start "GREATER"
    public final void mGREATER() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:591:9: ( '>' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:591:11: '>'
            {
            match('>'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "GREATER"

    // $ANTLR start "ASSIGN"
    public final void mASSIGN() throws RecognitionException {
        try {
            int _type = ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:593:9: ( '=' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:593:11: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ASSIGN"

    // $ANTLR start "EQUAL"
    public final void mEQUAL() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:596:9: ( '==' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:596:11: '=='
            {
            match("=="); 


            }

        }
        finally {
        }
    }
    // $ANTLR end "EQUAL"

    // $ANTLR start "NOTEQUAL"
    public final void mNOTEQUAL() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:599:13: ( '!=' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:599:15: '!='
            {
            match("!="); 


            }

        }
        finally {
        }
    }
    // $ANTLR end "NOTEQUAL"

    // $ANTLR start "LESSEQUAL"
    public final void mLESSEQUAL() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:602:13: ( '<=' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:602:15: '<='
            {
            match("<="); 


            }

        }
        finally {
        }
    }
    // $ANTLR end "LESSEQUAL"

    // $ANTLR start "GREATEREQUAL"
    public final void mGREATEREQUAL() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:605:17: ( '>=' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:605:19: '>='
            {
            match(">="); 


            }

        }
        finally {
        }
    }
    // $ANTLR end "GREATEREQUAL"

    // $ANTLR start "DOUBLESLASH"
    public final void mDOUBLESLASH() throws RecognitionException {
        try {
            int _type = DOUBLESLASH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:607:13: ( '//' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:607:15: '//'
            {
            match("//"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DOUBLESLASH"

    // $ANTLR start "DOT"
    public final void mDOT() throws RecognitionException {
        try {
            int _type = DOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:609:5: ( '.' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:609:7: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DOT"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:611:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:611:9: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:611:33: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='0' && LA2_0<='9')||(LA2_0>='A' && LA2_0<='Z')||LA2_0=='_'||(LA2_0>='a' && LA2_0<='z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "CONTINUED_LINE"
    public final void mCONTINUED_LINE() throws RecognitionException {
        try {
            int _type = CONTINUED_LINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:615:5: ( '\\\\' ( '\\r' )? '\\n' ( ' ' | '\\t' )* )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:615:7: '\\\\' ( '\\r' )? '\\n' ( ' ' | '\\t' )*
            {
            match('\\'); 
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:615:12: ( '\\r' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='\r') ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:615:13: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:615:25: ( ' ' | '\\t' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='\t'||LA4_0==' ') ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             _channel=HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "CONTINUED_LINE"

    // $ANTLR start "INDENTATION"
    public final void mINDENTATION() throws RecognitionException {
        try {
             
                level = 0;

            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:625:5: ({...}? => ( NEWLINE | ( ' ' )* COMMENT | ( ( ' ' )* ) ) )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:626:5: {...}? => ( NEWLINE | ( ' ' )* COMMENT | ( ( ' ' )* ) )
            {
            if ( !(( getCharPositionInLine()==0 )) ) {
                throw new FailedPredicateException(input, "INDENTATION", " getCharPositionInLine()==0 ");
            }
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:627:5: ( NEWLINE | ( ' ' )* COMMENT | ( ( ' ' )* ) )
            int alt7=3;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:627:6: NEWLINE
                    {
                    mNEWLINE(); 

                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:627:16: ( ' ' )* COMMENT
                    {
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:627:16: ( ' ' )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==' ') ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:627:17: ' '
                    	    {
                    	    match(' '); 

                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    mCOMMENT(); 
                    skip();

                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:628:5: ( ( ' ' )* )
                    {
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:628:5: ( ( ' ' )* )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:628:6: ( ' ' )*
                    {
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:628:6: ( ' ' )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==' ') ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:628:7: ' '
                    	    {
                    	    match(' '); 
                    	     level++; 

                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }

                      
                            if(indents.empty()) 
                                indents.push(0);

                            if (level > indents.peek().intValue())
                            {
                                emit(new ClassicToken(INDENT,"INDENT"));
                                indents.push(level);
                            } 
                      
                            else if(level < indents.peek().intValue())
                            {
                                while (level < indents.peek().intValue()) 
                                {
                                    indents.pop();
                                    emit(new ClassicToken(DEDENT,"DEDENT"));
                                 }
                            }
                            else if(level == indents.peek().intValue()) 
                                skip();
                        

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "INDENTATION"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:653:5: ( ( ' ' | '\\t' )+ )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:653:7: ( ' ' | '\\t' )+
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:653:7: ( ' ' | '\\t' )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0=='\t'||LA8_0==' ') ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "NEWLINE"
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:5: ( ( ( '\\r' )? '\\n' ) ( INDENTATION | EOF ) )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:9: ( ( '\\r' )? '\\n' ) ( INDENTATION | EOF )
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:9: ( ( '\\r' )? '\\n' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:10: ( '\\r' )? '\\n'
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:10: ( '\\r' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='\r') ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:11: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 

            }

            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:25: ( INDENTATION | EOF )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\n'||LA10_0=='\r'||LA10_0==' '||LA10_0=='/') && (( getCharPositionInLine()==0 ))) {
                alt10=1;
            }
            else if ( (( getCharPositionInLine()==0 )) ) {
                alt10=1;
            }
            else if ( (true) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:26: INDENTATION
                    {
                    mINDENTATION(); 

                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:657:40: EOF
                    {
                    match(EOF); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "NEWLINE"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:661:9: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:661:11: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
            {
            match("//"); 

            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:661:16: (~ ( '\\n' | '\\r' ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='\u0000' && LA11_0<='\t')||(LA11_0>='\u000B' && LA11_0<='\f')||(LA11_0>='\u000E' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:661:16: ~ ( '\\n' | '\\r' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:661:30: ( '\\r' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='\r') ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:661:30: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 
            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:665:5: ( ( DIGIT )+ )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:665:9: ( DIGIT )+
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:665:9: ( DIGIT )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>='0' && LA13_0<='9')) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:665:9: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:669:5: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:669:8: '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"'
            {
            match('\"'); 
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:669:12: ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )*
            loop14:
            do {
                int alt14=3;
                int LA14_0 = input.LA(1);

                if ( (LA14_0=='\\') ) {
                    alt14=1;
                }
                else if ( ((LA14_0>='\u0000' && LA14_0<='!')||(LA14_0>='#' && LA14_0<='[')||(LA14_0>=']' && LA14_0<='\uFFFF')) ) {
                    alt14=2;
                }


                switch (alt14) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:669:14: ESC_SEQ
            	    {
            	    mESC_SEQ(); 

            	    }
            	    break;
            	case 2 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:669:24: ~ ( '\\\\' | '\"' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "HEX_DIGIT"
    public final void mHEX_DIGIT() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:673:11: ( ( DIGIT | 'a' .. 'f' | 'A' .. 'F' ) )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:673:13: ( DIGIT | 'a' .. 'f' | 'A' .. 'F' )
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:673:13: ( DIGIT | 'a' .. 'f' | 'A' .. 'F' )
            int alt15=3;
            switch ( input.LA(1) ) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                {
                alt15=1;
                }
                break;
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
                {
                alt15=2;
                }
                break;
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
                {
                alt15=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:673:14: DIGIT
                    {
                    mDIGIT(); 

                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:673:20: 'a' .. 'f'
                    {
                    matchRange('a','f'); 

                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:673:29: 'A' .. 'F'
                    {
                    matchRange('A','F'); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "HEX_DIGIT"

    // $ANTLR start "DIGIT"
    public final void mDIGIT() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:676:9: ( ( '0' .. '9' ) )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:676:11: ( '0' .. '9' )
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:676:11: ( '0' .. '9' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:676:12: '0' .. '9'
            {
            matchRange('0','9'); 

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "DIGIT"

    // $ANTLR start "ESC_SEQ"
    public final void mESC_SEQ() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:680:5: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' ) | UNICODE_ESC | OCTAL_ESC )
            int alt16=3;
            int LA16_0 = input.LA(1);

            if ( (LA16_0=='\\') ) {
                switch ( input.LA(2) ) {
                case '\"':
                case '\'':
                case '\\':
                case 'b':
                case 'f':
                case 'n':
                case 'r':
                case 't':
                    {
                    alt16=1;
                    }
                    break;
                case 'u':
                    {
                    alt16=2;
                    }
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                    {
                    alt16=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 16, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:680:9: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' )
                    {
                    match('\\'); 
                    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}


                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:681:9: UNICODE_ESC
                    {
                    mUNICODE_ESC(); 

                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:682:9: OCTAL_ESC
                    {
                    mOCTAL_ESC(); 

                    }
                    break;

            }
        }
        finally {
        }
    }
    // $ANTLR end "ESC_SEQ"

    // $ANTLR start "OCTAL_ESC"
    public final void mOCTAL_ESC() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:687:5: ( '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) )
            int alt17=3;
            int LA17_0 = input.LA(1);

            if ( (LA17_0=='\\') ) {
                int LA17_1 = input.LA(2);

                if ( ((LA17_1>='0' && LA17_1<='3')) ) {
                    int LA17_2 = input.LA(3);

                    if ( ((LA17_2>='0' && LA17_2<='7')) ) {
                        int LA17_4 = input.LA(4);

                        if ( ((LA17_4>='0' && LA17_4<='7')) ) {
                            alt17=1;
                        }
                        else {
                            alt17=2;}
                    }
                    else {
                        alt17=3;}
                }
                else if ( ((LA17_1>='4' && LA17_1<='7')) ) {
                    int LA17_3 = input.LA(3);

                    if ( ((LA17_3>='0' && LA17_3<='7')) ) {
                        alt17=2;
                    }
                    else {
                        alt17=3;}
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 17, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:687:9: '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' )
                    {
                    match('\\'); 
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:687:14: ( '0' .. '3' )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:687:15: '0' .. '3'
                    {
                    matchRange('0','3'); 

                    }

                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:687:25: ( '0' .. '7' )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:687:26: '0' .. '7'
                    {
                    matchRange('0','7'); 

                    }

                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:687:36: ( '0' .. '7' )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:687:37: '0' .. '7'
                    {
                    matchRange('0','7'); 

                    }


                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:688:9: '\\\\' ( '0' .. '7' ) ( '0' .. '7' )
                    {
                    match('\\'); 
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:688:14: ( '0' .. '7' )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:688:15: '0' .. '7'
                    {
                    matchRange('0','7'); 

                    }

                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:688:25: ( '0' .. '7' )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:688:26: '0' .. '7'
                    {
                    matchRange('0','7'); 

                    }


                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:689:9: '\\\\' ( '0' .. '7' )
                    {
                    match('\\'); 
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:689:14: ( '0' .. '7' )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:689:15: '0' .. '7'
                    {
                    matchRange('0','7'); 

                    }


                    }
                    break;

            }
        }
        finally {
        }
    }
    // $ANTLR end "OCTAL_ESC"

    // $ANTLR start "UNICODE_ESC"
    public final void mUNICODE_ESC() throws RecognitionException {
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:694:5: ( '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:694:9: '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
            {
            match('\\'); 
            match('u'); 
            mHEX_DIGIT(); 
            mHEX_DIGIT(); 
            mHEX_DIGIT(); 
            mHEX_DIGIT(); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "UNICODE_ESC"

    public void mTokens() throws RecognitionException {
        // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:8: ( T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | LOGOP | COLON | COMMA | SEMI | PLUS | MINUS | STAR | SLASH | ISINT | ISSTRING | ISLIST | TOINT | TOSTRING | LENGTH | ADD | DEL | ASSIGN | DOUBLESLASH | DOT | ID | CONTINUED_LINE | WS | NEWLINE | COMMENT | INT | STRING )
        int alt18=47;
        alt18 = dfa18.predict(input);
        switch (alt18) {
            case 1 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:10: T__44
                {
                mT__44(); 

                }
                break;
            case 2 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:16: T__45
                {
                mT__45(); 

                }
                break;
            case 3 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:22: T__46
                {
                mT__46(); 

                }
                break;
            case 4 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:28: T__47
                {
                mT__47(); 

                }
                break;
            case 5 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:34: T__48
                {
                mT__48(); 

                }
                break;
            case 6 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:40: T__49
                {
                mT__49(); 

                }
                break;
            case 7 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:46: T__50
                {
                mT__50(); 

                }
                break;
            case 8 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:52: T__51
                {
                mT__51(); 

                }
                break;
            case 9 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:58: T__52
                {
                mT__52(); 

                }
                break;
            case 10 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:64: T__53
                {
                mT__53(); 

                }
                break;
            case 11 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:70: T__54
                {
                mT__54(); 

                }
                break;
            case 12 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:76: T__55
                {
                mT__55(); 

                }
                break;
            case 13 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:82: T__56
                {
                mT__56(); 

                }
                break;
            case 14 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:88: T__57
                {
                mT__57(); 

                }
                break;
            case 15 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:94: T__58
                {
                mT__58(); 

                }
                break;
            case 16 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:100: T__59
                {
                mT__59(); 

                }
                break;
            case 17 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:106: T__60
                {
                mT__60(); 

                }
                break;
            case 18 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:112: T__61
                {
                mT__61(); 

                }
                break;
            case 19 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:118: T__62
                {
                mT__62(); 

                }
                break;
            case 20 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:124: T__63
                {
                mT__63(); 

                }
                break;
            case 21 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:130: T__64
                {
                mT__64(); 

                }
                break;
            case 22 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:136: LOGOP
                {
                mLOGOP(); 

                }
                break;
            case 23 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:142: COLON
                {
                mCOLON(); 

                }
                break;
            case 24 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:148: COMMA
                {
                mCOMMA(); 

                }
                break;
            case 25 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:154: SEMI
                {
                mSEMI(); 

                }
                break;
            case 26 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:159: PLUS
                {
                mPLUS(); 

                }
                break;
            case 27 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:164: MINUS
                {
                mMINUS(); 

                }
                break;
            case 28 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:170: STAR
                {
                mSTAR(); 

                }
                break;
            case 29 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:175: SLASH
                {
                mSLASH(); 

                }
                break;
            case 30 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:181: ISINT
                {
                mISINT(); 

                }
                break;
            case 31 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:187: ISSTRING
                {
                mISSTRING(); 

                }
                break;
            case 32 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:196: ISLIST
                {
                mISLIST(); 

                }
                break;
            case 33 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:203: TOINT
                {
                mTOINT(); 

                }
                break;
            case 34 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:209: TOSTRING
                {
                mTOSTRING(); 

                }
                break;
            case 35 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:218: LENGTH
                {
                mLENGTH(); 

                }
                break;
            case 36 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:225: ADD
                {
                mADD(); 

                }
                break;
            case 37 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:229: DEL
                {
                mDEL(); 

                }
                break;
            case 38 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:233: ASSIGN
                {
                mASSIGN(); 

                }
                break;
            case 39 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:240: DOUBLESLASH
                {
                mDOUBLESLASH(); 

                }
                break;
            case 40 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:252: DOT
                {
                mDOT(); 

                }
                break;
            case 41 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:256: ID
                {
                mID(); 

                }
                break;
            case 42 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:259: CONTINUED_LINE
                {
                mCONTINUED_LINE(); 

                }
                break;
            case 43 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:274: WS
                {
                mWS(); 

                }
                break;
            case 44 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:277: NEWLINE
                {
                mNEWLINE(); 

                }
                break;
            case 45 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:285: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 46 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:293: INT
                {
                mINT(); 

                }
                break;
            case 47 :
                // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:1:297: STRING
                {
                mSTRING(); 

                }
                break;

        }

    }


    protected DFA7 dfa7 = new DFA7(this);
    protected DFA18 dfa18 = new DFA18(this);
    static final String DFA7_eotS =
        "\1\4\1\uffff\1\4\2\uffff";
    static final String DFA7_eofS =
        "\5\uffff";
    static final String DFA7_minS =
        "\1\12\1\uffff\1\40\2\uffff";
    static final String DFA7_maxS =
        "\1\57\1\uffff\1\57\2\uffff";
    static final String DFA7_acceptS =
        "\1\uffff\1\1\1\uffff\1\2\1\3";
    static final String DFA7_specialS =
        "\5\uffff}>";
    static final String[] DFA7_transitionS = {
            "\1\1\2\uffff\1\1\22\uffff\1\2\16\uffff\1\3",
            "",
            "\1\2\16\uffff\1\3",
            "",
            ""
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "627:5: ( NEWLINE | ( ' ' )* COMMENT | ( ( ' ' )* ) )";
        }
    }
    static final String DFA18_eotS =
        "\1\uffff\1\36\2\uffff\5\36\2\uffff\1\55\7\36\1\66\7\uffff\1\70"+
        "\2\36\6\uffff\7\36\3\uffff\1\104\1\106\6\36\1\uffff\1\115\1\uffff"+
        "\3\36\1\122\1\123\4\36\1\130\1\36\1\uffff\1\36\1\uffff\2\36\1\137"+
        "\3\36\2\uffff\2\36\1\145\2\uffff\1\146\2\36\1\151\1\uffff\1\36\2"+
        "\uffff\1\153\2\36\1\uffff\5\36\2\uffff\2\36\1\uffff\1\165\1\uffff"+
        "\1\166\1\36\1\170\1\36\1\172\1\uffff\1\36\1\174\1\36\2\uffff\1\36"+
        "\1\uffff\1\u0080\1\uffff\1\u0081\3\uffff\1\u0082\3\uffff";
    static final String DFA18_eofS =
        "\u0083\uffff";
    static final String DFA18_minS =
        "\1\11\1\145\2\uffff\1\141\1\164\1\141\1\145\1\162\2\uffff\1\142"+
        "\1\146\1\154\1\150\1\157\1\151\1\145\1\162\1\75\7\uffff\1\57\1\145"+
        "\1\144\6\uffff\1\146\2\151\1\162\1\163\1\164\1\151\3\uffff\2\60"+
        "\1\163\1\151\1\162\1\145\1\164\1\145\1\uffff\1\0\1\uffff\1\163\1"+
        "\156\1\144\2\60\1\156\1\164\1\151\1\145\1\60\1\156\1\uffff\1\41"+
        "\1\uffff\1\145\1\154\1\60\1\154\1\165\1\141\2\uffff\1\164\1\147"+
        "\1\60\2\uffff\1\60\1\143\1\156\1\60\1\uffff\1\164\2\uffff\1\60\1"+
        "\145\1\141\1\uffff\1\144\1\162\1\153\1\77\1\164\2\uffff\1\150\1"+
        "\147\1\uffff\1\60\1\uffff\1\60\1\143\1\60\1\156\1\60\1\uffff\1\150"+
        "\1\60\1\41\2\uffff\1\150\1\uffff\1\60\1\uffff\1\60\3\uffff\1\60"+
        "\3\uffff";
    static final String DFA18_maxS =
        "\1\172\1\145\2\uffff\1\141\1\167\1\141\1\145\1\162\2\uffff\2\156"+
        "\1\154\1\150\1\157\1\151\1\145\1\162\1\75\7\uffff\1\57\1\151\1\144"+
        "\6\uffff\1\154\2\151\1\162\1\163\1\164\1\151\3\uffff\2\172\1\163"+
        "\1\151\1\162\1\145\1\164\1\145\1\uffff\1\uffff\1\uffff\1\163\1\156"+
        "\1\144\2\172\1\156\1\164\1\151\1\145\1\172\1\156\1\uffff\1\77\1"+
        "\uffff\1\145\1\154\1\172\1\154\1\165\1\141\2\uffff\1\164\1\147\1"+
        "\172\2\uffff\1\172\1\143\1\156\1\172\1\uffff\1\164\2\uffff\1\172"+
        "\1\145\1\141\1\uffff\1\144\1\162\1\153\1\77\1\164\2\uffff\1\150"+
        "\1\147\1\uffff\1\172\1\uffff\1\172\1\143\1\172\1\156\1\172\1\uffff"+
        "\1\150\1\172\1\77\2\uffff\1\150\1\uffff\1\172\1\uffff\1\172\3\uffff"+
        "\1\172\3\uffff";
    static final String DFA18_acceptS =
        "\2\uffff\1\2\1\3\5\uffff\1\11\1\12\11\uffff\1\26\1\27\1\30\1\31"+
        "\1\32\1\33\1\34\3\uffff\1\51\1\52\1\53\1\54\1\56\1\57\7\uffff\1"+
        "\13\1\14\1\50\10\uffff\1\46\1\uffff\1\35\13\uffff\1\15\1\uffff\1"+
        "\22\6\uffff\1\47\1\55\3\uffff\1\1\1\45\4\uffff\1\7\1\uffff\1\36"+
        "\1\41\3\uffff\1\20\5\uffff\1\44\1\4\2\uffff\1\6\1\uffff\1\16\5\uffff"+
        "\1\40\3\uffff\1\10\1\17\1\uffff\1\23\1\uffff\1\25\1\uffff\1\5\1"+
        "\37\1\42\1\uffff\1\24\1\43\1\21";
    static final String DFA18_specialS =
        "\67\uffff\1\0\113\uffff}>";
    static final String[] DFA18_transitionS = {
            "\1\40\1\41\2\uffff\1\41\22\uffff\1\40\1\24\1\43\5\uffff\1\2"+
            "\1\3\1\32\1\30\1\26\1\31\1\13\1\33\12\42\1\25\1\27\1\24\1\23"+
            "\1\24\2\uffff\32\36\1\11\1\37\1\12\1\uffff\1\36\1\uffff\1\35"+
            "\1\22\1\6\1\1\1\15\1\17\1\7\1\36\1\14\2\36\1\34\1\4\2\36\1\10"+
            "\1\36\1\21\1\5\3\36\1\16\1\36\1\20\1\36",
            "\1\44",
            "",
            "",
            "\1\45",
            "\1\47\2\uffff\1\46",
            "\1\50",
            "\1\51",
            "\1\52",
            "",
            "",
            "\1\54\13\uffff\1\53",
            "\1\56\7\uffff\1\57",
            "\1\60",
            "\1\61",
            "\1\62",
            "\1\63",
            "\1\64",
            "\1\65",
            "\1\24",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\67",
            "\1\72\3\uffff\1\71",
            "\1\73",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\74\5\uffff\1\75",
            "\1\76",
            "\1\77",
            "\1\100",
            "\1\101",
            "\1\102",
            "\1\103",
            "",
            "",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\23\36\1\105\6\36",
            "\1\107",
            "\1\110",
            "\1\111",
            "\1\112",
            "\1\113",
            "\1\114",
            "",
            "\0\116",
            "",
            "\1\117",
            "\1\120",
            "\1\121",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\124",
            "\1\125",
            "\1\126",
            "\1\127",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\131",
            "",
            "\1\133\35\uffff\1\132",
            "",
            "\1\134",
            "\1\135",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\4\36\1\136\25\36",
            "\1\140",
            "\1\141",
            "\1\142",
            "",
            "",
            "\1\143",
            "\1\144",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\147",
            "\1\150",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\152",
            "",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\154",
            "\1\155",
            "",
            "\1\156",
            "\1\157",
            "\1\160",
            "\1\161",
            "\1\162",
            "",
            "",
            "\1\163",
            "\1\164",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\167",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\171",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\173",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\176\35\uffff\1\175",
            "",
            "",
            "\1\177",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "",
            ""
    };

    static final short[] DFA18_eot = DFA.unpackEncodedString(DFA18_eotS);
    static final short[] DFA18_eof = DFA.unpackEncodedString(DFA18_eofS);
    static final char[] DFA18_min = DFA.unpackEncodedStringToUnsignedChars(DFA18_minS);
    static final char[] DFA18_max = DFA.unpackEncodedStringToUnsignedChars(DFA18_maxS);
    static final short[] DFA18_accept = DFA.unpackEncodedString(DFA18_acceptS);
    static final short[] DFA18_special = DFA.unpackEncodedString(DFA18_specialS);
    static final short[][] DFA18_transition;

    static {
        int numStates = DFA18_transitionS.length;
        DFA18_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA18_transition[i] = DFA.unpackEncodedString(DFA18_transitionS[i]);
        }
    }

    class DFA18 extends DFA {

        public DFA18(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 18;
            this.eot = DFA18_eot;
            this.eof = DFA18_eof;
            this.min = DFA18_min;
            this.max = DFA18_max;
            this.accept = DFA18_accept;
            this.special = DFA18_special;
            this.transition = DFA18_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | LOGOP | COLON | COMMA | SEMI | PLUS | MINUS | STAR | SLASH | ISINT | ISSTRING | ISLIST | TOINT | TOSTRING | LENGTH | ADD | DEL | ASSIGN | DOUBLESLASH | DOT | ID | CONTINUED_LINE | WS | NEWLINE | COMMENT | INT | STRING );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA18_55 = input.LA(1);

                        s = -1;
                        if ( ((LA18_55>='\u0000' && LA18_55<='\uFFFF')) ) {s = 78;}

                        else s = 77;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 18, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}