// $ANTLR 3.3 Nov 30, 2010 12:50:56 D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g 2013-06-19 01:27:59

    package by.bsuir.yapis.language.main;
    import by.bsuir.yapis.language.main.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.antlr.stringtemplate.*;
import org.antlr.stringtemplate.language.*;
import java.util.HashMap;
public class SimpleListsParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "INDENT", "DEDENT", "NEWLINE", "ID", "INT", "ASSIGN", "SEMI", "LOGOP", "ISINT", "ISSTRING", "ISLIST", "TOINT", "TOSTRING", "LENGTH", "DEL", "ADD", "STRING", "EQUAL", "LESS", "GREATER", "NOTEQUAL", "LESSEQUAL", "GREATEREQUAL", "COLON", "COMMA", "PLUS", "MINUS", "STAR", "SLASH", "DOUBLESLASH", "DOT", "CONTINUED_LINE", "COMMENT", "INDENTATION", "WS", "DIGIT", "ESC_SEQ", "HEX_DIGIT", "UNICODE_ESC", "OCTAL_ESC", "'def'", "'('", "')'", "'main'", "'switch'", "'case'", "'get'", "'print'", "'['", "']'", "'.next'", "'.begin'", "'if'", "'else'", "'while'", "'for'", "'foreach'", "'in'", "'yield'", "'return'", "'break'"
    };
    public static final int EOF=-1;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int INDENT=4;
    public static final int DEDENT=5;
    public static final int NEWLINE=6;
    public static final int ID=7;
    public static final int INT=8;
    public static final int ASSIGN=9;
    public static final int SEMI=10;
    public static final int LOGOP=11;
    public static final int ISINT=12;
    public static final int ISSTRING=13;
    public static final int ISLIST=14;
    public static final int TOINT=15;
    public static final int TOSTRING=16;
    public static final int LENGTH=17;
    public static final int DEL=18;
    public static final int ADD=19;
    public static final int STRING=20;
    public static final int EQUAL=21;
    public static final int LESS=22;
    public static final int GREATER=23;
    public static final int NOTEQUAL=24;
    public static final int LESSEQUAL=25;
    public static final int GREATEREQUAL=26;
    public static final int COLON=27;
    public static final int COMMA=28;
    public static final int PLUS=29;
    public static final int MINUS=30;
    public static final int STAR=31;
    public static final int SLASH=32;
    public static final int DOUBLESLASH=33;
    public static final int DOT=34;
    public static final int CONTINUED_LINE=35;
    public static final int COMMENT=36;
    public static final int INDENTATION=37;
    public static final int WS=38;
    public static final int DIGIT=39;
    public static final int ESC_SEQ=40;
    public static final int HEX_DIGIT=41;
    public static final int UNICODE_ESC=42;
    public static final int OCTAL_ESC=43;

    // delegates
    // delegators


        public SimpleListsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public SimpleListsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        
    protected StringTemplateGroup templateLib =
      new StringTemplateGroup("SimpleListsParserTemplates", AngleBracketTemplateLexer.class);

    public void setTemplateLib(StringTemplateGroup templateLib) {
      this.templateLib = templateLib;
    }
    public StringTemplateGroup getTemplateLib() {
      return templateLib;
    }
    /** allows convenient multi-value initialization:
     *  "new STAttrMap().put(...).put(...)"
     */
    public static class STAttrMap extends HashMap {
      public STAttrMap put(String attrName, Object value) {
        super.put(attrName, value);
        return this;
      }
      public STAttrMap put(String attrName, int value) {
        super.put(attrName, new Integer(value));
        return this;
      }
    }

    public String[] getTokenNames() { return SimpleListsParser.tokenNames; }
    public String getGrammarFileName() { return "D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g"; }


        public List<Error> errors = new ArrayList<Error>();


    protected static class program_scope {
        List funcTempl;
        Functions functions;
        Functions initCoroutines;
        Blocks blocks;
    }
    protected Stack program_stack = new Stack();

    public static class program_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "program"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:56:1: program : functions ( NEWLINE )* main EOF -> program(functions=$program::funcTemplmain=$main.st);
    public final SimpleListsParser.program_return program() throws RecognitionException {
        program_stack.push(new program_scope());
        SimpleListsParser.program_return retval = new SimpleListsParser.program_return();
        retval.start = input.LT(1);

        SimpleListsParser.main_return main1 = null;



            ((program_scope)program_stack.peek()).blocks = new Blocks();
            ((program_scope)program_stack.peek()).functions = new Functions();
            ((program_scope)program_stack.peek()).initCoroutines = new Functions();
            ((program_scope)program_stack.peek()).funcTempl = new ArrayList();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:71:5: ( functions ( NEWLINE )* main EOF -> program(functions=$program::funcTemplmain=$main.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:71:8: functions ( NEWLINE )* main EOF
            {
            pushFollow(FOLLOW_functions_in_program110);
            functions();

            state._fsp--;
            if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:71:18: ( NEWLINE )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==NEWLINE) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: NEWLINE
            	    {
            	    match(input,NEWLINE,FOLLOW_NEWLINE_in_program112); if (state.failed) return retval;

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            pushFollow(FOLLOW_main_in_program115);
            main1=main();

            state._fsp--;
            if (state.failed) return retval;
            match(input,EOF,FOLLOW_EOF_in_program117); if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 73:5: -> program(functions=$program::funcTemplmain=$main.st)
              {
                  retval.st = templateLib.getInstanceOf("program",
                new STAttrMap().put("functions", ((program_scope)program_stack.peek()).funcTempl).put("main", (main1!=null?main1.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            program_stack.pop();
        }
        return retval;
    }
    // $ANTLR end "program"

    public static class functions_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "functions"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:76:1: functions : ( ( NEWLINE )* function )* ;
    public final SimpleListsParser.functions_return functions() throws RecognitionException {
        SimpleListsParser.functions_return retval = new SimpleListsParser.functions_return();
        retval.start = input.LT(1);

        SimpleListsParser.function_return function2 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:77:5: ( ( ( NEWLINE )* function )* )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:77:8: ( ( NEWLINE )* function )*
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:77:8: ( ( NEWLINE )* function )*
            loop3:
            do {
                int alt3=2;
                alt3 = dfa3.predict(input);
                switch (alt3) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:77:9: ( NEWLINE )* function
            	    {
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:77:9: ( NEWLINE )*
            	    loop2:
            	    do {
            	        int alt2=2;
            	        int LA2_0 = input.LA(1);

            	        if ( (LA2_0==NEWLINE) ) {
            	            alt2=1;
            	        }


            	        switch (alt2) {
            	    	case 1 :
            	    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: NEWLINE
            	    	    {
            	    	    match(input,NEWLINE,FOLLOW_NEWLINE_in_functions161); if (state.failed) return retval;

            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop2;
            	        }
            	    } while (true);

            	    pushFollow(FOLLOW_function_in_functions164);
            	    function2=function();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {

            	                ((program_scope)program_stack.peek()).funcTempl.add((function2!=null?function2.st:null));
            	             
            	    }

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "functions"

    protected static class function_scope {
        Function func;
        List paramTempl;
        FunReturn returnType;
        boolean isCor;
    }
    protected Stack function_stack = new Stack();

    public static class function_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "function"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:84:1: function : 'def' ID '(' params ')' block[$function::func.getParams()] -> function(id=$ID.textparams=$function::paramTemplblock=$block.stisCor=$function::isCor);
    public final SimpleListsParser.function_return function() throws RecognitionException {
        function_stack.push(new function_scope());
        SimpleListsParser.function_return retval = new SimpleListsParser.function_return();
        retval.start = input.LT(1);

        Token ID3=null;
        SimpleListsParser.block_return block4 = null;



            ((function_scope)function_stack.peek()).func = new Function();
            ((function_scope)function_stack.peek()).paramTempl = new ArrayList();
            ((function_scope)function_stack.peek()).returnType = FunReturn.NONE;
            ((function_scope)function_stack.peek()).isCor = false;
            

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:100:5: ( 'def' ID '(' params ')' block[$function::func.getParams()] -> function(id=$ID.textparams=$function::paramTemplblock=$block.stisCor=$function::isCor))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:100:7: 'def' ID '(' params ')' block[$function::func.getParams()]
            {
            match(input,44,FOLLOW_44_in_function218); if (state.failed) return retval;
            ID3=(Token)match(input,ID,FOLLOW_ID_in_function220); if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                      if(((program_scope)program_stack.peek()).functions.contains((ID3!=null?ID3.getText():null))) {
                          errors.add(new Error("line " + (ID3!=null?ID3.getLine():0) + " - multiple function declaration: " + (ID3!=null?ID3.getText():null)));
                      }
                      else {
                          ((function_scope)function_stack.peek()).func.setId((ID3!=null?ID3.getText():null));
                          ((program_scope)program_stack.peek()).functions.add(((function_scope)function_stack.peek()).func);
                      }
                      ((function_scope)function_stack.peek()).func.setReturnValue(FunReturn.RETURN);
                    
            }
            match(input,45,FOLLOW_45_in_function233); if (state.failed) return retval;
            pushFollow(FOLLOW_params_in_function235);
            params();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_function237); if (state.failed) return retval;
            pushFollow(FOLLOW_block_in_function239);
            block4=block(((function_scope)function_stack.peek()).func.getParams());

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                      ((function_scope)function_stack.peek()).func.setReturnValue(((function_scope)function_stack.peek()).returnType);
                  
            }


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 115:7: -> function(id=$ID.textparams=$function::paramTemplblock=$block.stisCor=$function::isCor)
              {
                  retval.st = templateLib.getInstanceOf("function",
                new STAttrMap().put("id", (ID3!=null?ID3.getText():null)).put("params", ((function_scope)function_stack.peek()).paramTempl).put("block", (block4!=null?block4.st:null)).put("isCor", ((function_scope)function_stack.peek()).isCor));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            function_stack.pop();
        }
        return retval;
    }
    // $ANTLR end "function"

    public static class params_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "params"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:118:1: params : (par= param (comma= ',' par_2= param )* )? ;
    public final SimpleListsParser.params_return params() throws RecognitionException {
        SimpleListsParser.params_return retval = new SimpleListsParser.params_return();
        retval.start = input.LT(1);

        Token comma=null;
        SimpleListsParser.param_return par = null;

        SimpleListsParser.param_return par_2 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:118:9: ( (par= param (comma= ',' par_2= param )* )? )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:118:11: (par= param (comma= ',' par_2= param )* )?
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:118:11: (par= param (comma= ',' par_2= param )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:118:13: par= param (comma= ',' par_2= param )*
                    {
                    pushFollow(FOLLOW_param_in_params313);
                    par=param();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                       
                                    ((function_scope)function_stack.peek()).func.addParam((par!=null?input.toString(par.start,par.stop):null));
                                    ((function_scope)function_stack.peek()).paramTempl.add((par!=null?input.toString(par.start,par.stop):null));
                                
                    }
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:124:11: (comma= ',' par_2= param )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==COMMA) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:124:13: comma= ',' par_2= param
                    	    {
                    	    comma=(Token)match(input,COMMA,FOLLOW_COMMA_in_params355); if (state.failed) return retval;
                    	    pushFollow(FOLLOW_param_in_params361);
                    	    par_2=param();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {

                    	                    if (((function_scope)function_stack.peek()).func.getParams().contains((par_2!=null?input.toString(par_2.start,par_2.stop):null))) {
                    	                        errors.add(new Error("line " + (comma!=null?comma.getLine():0) + " - duplicate parameter: " + (par_2!=null?input.toString(par_2.start,par_2.stop):null)));
                    	                    }
                    	                    else {
                    	                        ((function_scope)function_stack.peek()).func.addParam((par_2!=null?input.toString(par_2.start,par_2.stop):null));
                    	                        ((function_scope)function_stack.peek()).paramTempl.add((par_2!=null?input.toString(par_2.start,par_2.stop):null));
                    	                    }
                    	                
                    	    }

                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "params"

    public static class param_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "param"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:137:1: param : ID ;
    public final SimpleListsParser.param_return param() throws RecognitionException {
        SimpleListsParser.param_return retval = new SimpleListsParser.param_return();
        retval.start = input.LT(1);

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:137:9: ( ID )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:137:11: ID
            {
            match(input,ID,FOLLOW_ID_in_param410); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "param"

    public static class declarator_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "declarator"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:140:1: declarator : ID ;
    public final SimpleListsParser.declarator_return declarator() throws RecognitionException {
        SimpleListsParser.declarator_return retval = new SimpleListsParser.declarator_return();
        retval.start = input.LT(1);

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:141:5: ( ID )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:141:7: ID
            {
            match(input,ID,FOLLOW_ID_in_declarator427); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "declarator"

    public static class main_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "main"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:144:1: main : 'main' '(' params ')' block[new ArrayList<String>()] EOF -> main(block=$block.st);
    public final SimpleListsParser.main_return main() throws RecognitionException {
        SimpleListsParser.main_return retval = new SimpleListsParser.main_return();
        retval.start = input.LT(1);

        SimpleListsParser.block_return block5 = null;



            ((program_scope)program_stack.peek()).blocks.addBlock(new Block());

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:153:5: ( 'main' '(' params ')' block[new ArrayList<String>()] EOF -> main(block=$block.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:153:7: 'main' '(' params ')' block[new ArrayList<String>()] EOF
            {
            match(input,47,FOLLOW_47_in_main459); if (state.failed) return retval;
            match(input,45,FOLLOW_45_in_main461); if (state.failed) return retval;
            pushFollow(FOLLOW_params_in_main463);
            params();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_main465); if (state.failed) return retval;
            pushFollow(FOLLOW_block_in_main467);
            block5=block(new ArrayList<String>());

            state._fsp--;
            if (state.failed) return retval;
            match(input,EOF,FOLLOW_EOF_in_main470); if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 154:5: -> main(block=$block.st)
              {
                  retval.st = templateLib.getInstanceOf("main",
                new STAttrMap().put("block", (block5!=null?block5.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

                  ((program_scope)program_stack.peek()).blocks.pop();

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "main"

    public static class stmt_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:157:1: stmt : ( stmt_assign | stmt_func | stmt_yield | stmt_return | stmt_break | stmt_if | stmt_print | stmt_while | stmt_for | stmt_for_list | stmt_get | stmt_switch | stmt_coroutine | stmt_next );
    public final SimpleListsParser.stmt_return stmt() throws RecognitionException {
        SimpleListsParser.stmt_return retval = new SimpleListsParser.stmt_return();
        retval.start = input.LT(1);

        SimpleListsParser.stmt_assign_return stmt_assign6 = null;

        SimpleListsParser.stmt_func_return stmt_func7 = null;

        SimpleListsParser.stmt_yield_return stmt_yield8 = null;

        SimpleListsParser.stmt_return_return stmt_return9 = null;

        SimpleListsParser.stmt_break_return stmt_break10 = null;

        SimpleListsParser.stmt_if_return stmt_if11 = null;

        SimpleListsParser.stmt_print_return stmt_print12 = null;

        SimpleListsParser.stmt_while_return stmt_while13 = null;

        SimpleListsParser.stmt_for_return stmt_for14 = null;

        SimpleListsParser.stmt_for_list_return stmt_for_list15 = null;

        SimpleListsParser.stmt_get_return stmt_get16 = null;

        SimpleListsParser.stmt_switch_return stmt_switch17 = null;

        SimpleListsParser.stmt_coroutine_return stmt_coroutine18 = null;

        SimpleListsParser.stmt_next_return stmt_next19 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:157:9: ( stmt_assign | stmt_func | stmt_yield | stmt_return | stmt_break | stmt_if | stmt_print | stmt_while | stmt_for | stmt_for_list | stmt_get | stmt_switch | stmt_coroutine | stmt_next )
            int alt6=14;
            alt6 = dfa6.predict(input);
            switch (alt6) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:157:11: stmt_assign
                    {
                    pushFollow(FOLLOW_stmt_assign_in_stmt510);
                    stmt_assign6=stmt_assign();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_assign6!=null?stmt_assign6.st:null));
                    }

                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:158:11: stmt_func
                    {
                    pushFollow(FOLLOW_stmt_func_in_stmt527);
                    stmt_func7=stmt_func();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_func7!=null?stmt_func7.st:null));
                    }

                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:159:11: stmt_yield
                    {
                    pushFollow(FOLLOW_stmt_yield_in_stmt544);
                    stmt_yield8=stmt_yield();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_yield8!=null?stmt_yield8.st:null));
                    }

                    }
                    break;
                case 4 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:160:11: stmt_return
                    {
                    pushFollow(FOLLOW_stmt_return_in_stmt560);
                    stmt_return9=stmt_return();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_return9!=null?stmt_return9.st:null));
                    }

                    }
                    break;
                case 5 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:161:11: stmt_break
                    {
                    pushFollow(FOLLOW_stmt_break_in_stmt576);
                    stmt_break10=stmt_break();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_break10!=null?stmt_break10.st:null));
                    }

                    }
                    break;
                case 6 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:162:11: stmt_if
                    {
                    pushFollow(FOLLOW_stmt_if_in_stmt592);
                    stmt_if11=stmt_if();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_if11!=null?stmt_if11.st:null));
                    }

                    }
                    break;
                case 7 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:163:11: stmt_print
                    {
                    pushFollow(FOLLOW_stmt_print_in_stmt609);
                    stmt_print12=stmt_print();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_print12!=null?stmt_print12.st:null));
                    }

                    }
                    break;
                case 8 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:164:11: stmt_while
                    {
                    pushFollow(FOLLOW_stmt_while_in_stmt626);
                    stmt_while13=stmt_while();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_while13!=null?stmt_while13.st:null));
                    }

                    }
                    break;
                case 9 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:165:11: stmt_for
                    {
                    pushFollow(FOLLOW_stmt_for_in_stmt643);
                    stmt_for14=stmt_for();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_for14!=null?stmt_for14.st:null));
                    }

                    }
                    break;
                case 10 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:166:11: stmt_for_list
                    {
                    pushFollow(FOLLOW_stmt_for_list_in_stmt659);
                    stmt_for_list15=stmt_for_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_for_list15!=null?stmt_for_list15.st:null));
                    }

                    }
                    break;
                case 11 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:167:11: stmt_get
                    {
                    pushFollow(FOLLOW_stmt_get_in_stmt676);
                    stmt_get16=stmt_get();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_get16!=null?stmt_get16.st:null));
                    }

                    }
                    break;
                case 12 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:168:11: stmt_switch
                    {
                    pushFollow(FOLLOW_stmt_switch_in_stmt692);
                    stmt_switch17=stmt_switch();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_switch17!=null?stmt_switch17.st:null));
                    }

                    }
                    break;
                case 13 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:169:11: stmt_coroutine
                    {
                    pushFollow(FOLLOW_stmt_coroutine_in_stmt707);
                    stmt_coroutine18=stmt_coroutine();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_coroutine18!=null?stmt_coroutine18.st:null));
                    }

                    }
                    break;
                case 14 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:170:11: stmt_next
                    {
                    pushFollow(FOLLOW_stmt_next_in_stmt722);
                    stmt_next19=stmt_next();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      ((block_scope)block_stack.peek()).stmtsTempl.add((stmt_next19!=null?stmt_next19.st:null));
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt"

    protected static class stmt_switch_scope {
        List caseTempls;
    }
    protected Stack stmt_switch_stack = new Stack();

    public static class stmt_switch_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_switch"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:173:1: stmt_switch : 'switch' '(' expr ')' INDENT ( case_stmt ( stmt_break )? )+ DEDENT -> stmt_switch(cases=$stmt_switch::caseTemplsexpr=$expr.st);
    public final SimpleListsParser.stmt_switch_return stmt_switch() throws RecognitionException {
        stmt_switch_stack.push(new stmt_switch_scope());
        SimpleListsParser.stmt_switch_return retval = new SimpleListsParser.stmt_switch_return();
        retval.start = input.LT(1);

        SimpleListsParser.case_stmt_return case_stmt20 = null;

        SimpleListsParser.stmt_break_return stmt_break21 = null;

        SimpleListsParser.expr_return expr22 = null;



            ((stmt_switch_scope)stmt_switch_stack.peek()).caseTempls = new ArrayList();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:182:5: ( 'switch' '(' expr ')' INDENT ( case_stmt ( stmt_break )? )+ DEDENT -> stmt_switch(cases=$stmt_switch::caseTemplsexpr=$expr.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:182:7: 'switch' '(' expr ')' INDENT ( case_stmt ( stmt_break )? )+ DEDENT
            {
            match(input,48,FOLLOW_48_in_stmt_switch759); if (state.failed) return retval;
            match(input,45,FOLLOW_45_in_stmt_switch761); if (state.failed) return retval;
            pushFollow(FOLLOW_expr_in_stmt_switch763);
            expr22=expr();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_stmt_switch765); if (state.failed) return retval;
            match(input,INDENT,FOLLOW_INDENT_in_stmt_switch767); if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:182:36: ( case_stmt ( stmt_break )? )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==49) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:182:37: case_stmt ( stmt_break )?
            	    {
            	    pushFollow(FOLLOW_case_stmt_in_stmt_switch770);
            	    case_stmt20=case_stmt();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	      ((stmt_switch_scope)stmt_switch_stack.peek()).caseTempls.add((case_stmt20!=null?case_stmt20.st:null));
            	    }
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:182:94: ( stmt_break )?
            	    int alt7=2;
            	    int LA7_0 = input.LA(1);

            	    if ( (LA7_0==64) ) {
            	        alt7=1;
            	    }
            	    switch (alt7) {
            	        case 1 :
            	            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: stmt_break
            	            {
            	            pushFollow(FOLLOW_stmt_break_in_stmt_switch774);
            	            stmt_break21=stmt_break();

            	            state._fsp--;
            	            if (state.failed) return retval;

            	            }
            	            break;

            	    }

            	    if ( state.backtracking==0 ) {
            	      ((stmt_switch_scope)stmt_switch_stack.peek()).caseTempls.add((stmt_break21!=null?stmt_break21.st:null));
            	    }

            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
            	    if (state.backtracking>0) {state.failed=true; return retval;}
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            match(input,DEDENT,FOLLOW_DEDENT_in_stmt_switch781); if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 183:5: -> stmt_switch(cases=$stmt_switch::caseTemplsexpr=$expr.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_switch",
                new STAttrMap().put("cases", ((stmt_switch_scope)stmt_switch_stack.peek()).caseTempls).put("expr", (expr22!=null?expr22.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            stmt_switch_stack.pop();
        }
        return retval;
    }
    // $ANTLR end "stmt_switch"

    public static class case_stmt_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "case_stmt"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:186:1: case_stmt : 'case' INT block[new ArrayList<String>()] -> case_stmt(num=$INT.textblock=$block.st);
    public final SimpleListsParser.case_stmt_return case_stmt() throws RecognitionException {
        SimpleListsParser.case_stmt_return retval = new SimpleListsParser.case_stmt_return();
        retval.start = input.LT(1);

        Token INT23=null;
        SimpleListsParser.block_return block24 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:186:10: ( 'case' INT block[new ArrayList<String>()] -> case_stmt(num=$INT.textblock=$block.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:186:12: 'case' INT block[new ArrayList<String>()]
            {
            match(input,49,FOLLOW_49_in_case_stmt814); if (state.failed) return retval;
            INT23=(Token)match(input,INT,FOLLOW_INT_in_case_stmt816); if (state.failed) return retval;
            pushFollow(FOLLOW_block_in_case_stmt818);
            block24=block(new ArrayList<String>());

            state._fsp--;
            if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 187:5: -> case_stmt(num=$INT.textblock=$block.st)
              {
                  retval.st = templateLib.getInstanceOf("case_stmt",
                new STAttrMap().put("num", (INT23!=null?INT23.getText():null)).put("block", (block24!=null?block24.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "case_stmt"

    public static class stmt_get_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_get"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:190:1: stmt_get : 'get' ID -> stmt_get(ID=$ID.textdecl=decl);
    public final SimpleListsParser.stmt_get_return stmt_get() throws RecognitionException {
        SimpleListsParser.stmt_get_return retval = new SimpleListsParser.stmt_get_return();
        retval.start = input.LT(1);

        Token ID25=null;


            boolean decl = true;

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:195:5: ( 'get' ID -> stmt_get(ID=$ID.textdecl=decl))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:195:7: 'get' ID
            {
            match(input,50,FOLLOW_50_in_stmt_get870); if (state.failed) return retval;
            ID25=(Token)match(input,ID,FOLLOW_ID_in_stmt_get872); if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                  if(((program_scope)program_stack.peek()).blocks.checkIdInBlocks((ID25!=null?ID25.getText():null)) || ((program_scope)program_stack.peek()).blocks.checkIdForVars((ID25!=null?ID25.getText():null)))
                          decl = false;
                      else ((program_scope)program_stack.peek()).blocks.addVarToTopBlock((ID25!=null?ID25.getText():null));
                  
            }


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 201:5: -> stmt_get(ID=$ID.textdecl=decl)
              {
                  retval.st = templateLib.getInstanceOf("stmt_get",
                new STAttrMap().put("ID", (ID25!=null?ID25.getText():null)).put("decl", decl));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_get"

    public static class stmt_print_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_print"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:204:1: stmt_print : 'print' expr -> stmt_print(expr=$expr.st);
    public final SimpleListsParser.stmt_print_return stmt_print() throws RecognitionException {
        SimpleListsParser.stmt_print_return retval = new SimpleListsParser.stmt_print_return();
        retval.start = input.LT(1);

        SimpleListsParser.expr_return expr26 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:205:5: ( 'print' expr -> stmt_print(expr=$expr.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:205:7: 'print' expr
            {
            match(input,51,FOLLOW_51_in_stmt_print921); if (state.failed) return retval;
            pushFollow(FOLLOW_expr_in_stmt_print923);
            expr26=expr();

            state._fsp--;
            if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 206:5: -> stmt_print(expr=$expr.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_print",
                new STAttrMap().put("expr", (expr26!=null?expr26.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_print"

    public static class stmt_assign_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_assign"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:209:1: stmt_assign : (id= ID | listId= ID '[' expr ']' ) ASSIGN value -> stmt_assign(id=varexpr=$value.stdecl=decl);
    public final SimpleListsParser.stmt_assign_return stmt_assign() throws RecognitionException {
        SimpleListsParser.stmt_assign_return retval = new SimpleListsParser.stmt_assign_return();
        retval.start = input.LT(1);

        Token id=null;
        Token listId=null;
        SimpleListsParser.expr_return expr27 = null;

        SimpleListsParser.value_return value28 = null;



            boolean decl = true;
            String var = new String();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:215:5: ( (id= ID | listId= ID '[' expr ']' ) ASSIGN value -> stmt_assign(id=varexpr=$value.stdecl=decl))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:215:7: (id= ID | listId= ID '[' expr ']' ) ASSIGN value
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:215:7: (id= ID | listId= ID '[' expr ']' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==ID) ) {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==52) ) {
                    alt9=2;
                }
                else if ( (LA9_1==ASSIGN) ) {
                    alt9=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:215:8: id= ID
                    {
                    id=(Token)match(input,ID,FOLLOW_ID_in_stmt_assign968); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              if(((program_scope)program_stack.peek()).blocks.checkIdInBlocks((id!=null?id.getText():null)) || ((program_scope)program_stack.peek()).blocks.checkIdForVars((id!=null?id.getText():null)))
                                  decl = false;
                              else ((program_scope)program_stack.peek()).blocks.addVarToTopBlock((id!=null?id.getText():null));
                              var = (id!=null?id.getText():null);    
                          
                    }

                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:222:7: listId= ID '[' expr ']'
                    {
                    listId=(Token)match(input,ID,FOLLOW_ID_in_stmt_assign987); if (state.failed) return retval;
                    match(input,52,FOLLOW_52_in_stmt_assign989); if (state.failed) return retval;
                    pushFollow(FOLLOW_expr_in_stmt_assign991);
                    expr27=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    match(input,53,FOLLOW_53_in_stmt_assign993); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              decl = false;
                              var = (listId!=null?listId.getText():null) + ".get(" + (expr27!=null?expr27.st:null).toString() + ")";
                          
                    }

                    }
                    break;

            }

            match(input,ASSIGN,FOLLOW_ASSIGN_in_stmt_assign1008); if (state.failed) return retval;
            pushFollow(FOLLOW_value_in_stmt_assign1010);
            value28=value();

            state._fsp--;
            if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 228:5: -> stmt_assign(id=varexpr=$value.stdecl=decl)
              {
                  retval.st = templateLib.getInstanceOf("stmt_assign",
                new STAttrMap().put("id", var).put("expr", (value28!=null?value28.st:null)).put("decl", decl));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_assign"

    public static class value_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "value"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:231:2: value : ( expr -> {$expr.st} | list -> {$list.st});
    public final SimpleListsParser.value_return value() throws RecognitionException {
        SimpleListsParser.value_return retval = new SimpleListsParser.value_return();
        retval.start = input.LT(1);

        SimpleListsParser.expr_return expr29 = null;

        SimpleListsParser.list_return list30 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:232:5: ( expr -> {$expr.st} | list -> {$list.st})
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( ((LA10_0>=ID && LA10_0<=INT)||LA10_0==STRING||LA10_0==45) ) {
                alt10=1;
            }
            else if ( (LA10_0==52) ) {
                int LA10_3 = input.LA(2);

                if ( (synpred22_SimpleLists()) ) {
                    alt10=1;
                }
                else if ( (true) ) {
                    alt10=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 3, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:232:7: expr
                    {
                    pushFollow(FOLLOW_expr_in_value1057);
                    expr29=expr();

                    state._fsp--;
                    if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 233:7: -> {$expr.st}
                      {
                          retval.st = (expr29!=null?expr29.st:null);
                      }

                    }
                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:234:7: list
                    {
                    pushFollow(FOLLOW_list_in_value1078);
                    list30=list();

                    state._fsp--;
                    if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 235:7: -> {$list.st}
                      {
                          retval.st = (list30!=null?list30.st:null);
                      }

                    }
                    }
                    break;

            }
            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "value"

    public static class list_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "list"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:238:1: list : ( '[' ( (expr_1= expr ) ( ',' (expr_2= expr ) )* )? ']' ) -> list(exprs=exprs);
    public final SimpleListsParser.list_return list() throws RecognitionException {
        SimpleListsParser.list_return retval = new SimpleListsParser.list_return();
        retval.start = input.LT(1);

        SimpleListsParser.expr_return expr_1 = null;

        SimpleListsParser.expr_return expr_2 = null;



            List exprs = new ArrayList();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:243:5: ( ( '[' ( (expr_1= expr ) ( ',' (expr_2= expr ) )* )? ']' ) -> list(exprs=exprs))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:243:6: ( '[' ( (expr_1= expr ) ( ',' (expr_2= expr ) )* )? ']' )
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:243:6: ( '[' ( (expr_1= expr ) ( ',' (expr_2= expr ) )* )? ']' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:243:7: '[' ( (expr_1= expr ) ( ',' (expr_2= expr ) )* )? ']'
            {
            match(input,52,FOLLOW_52_in_list1109); if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:243:11: ( (expr_1= expr ) ( ',' (expr_2= expr ) )* )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=ID && LA12_0<=INT)||LA12_0==STRING||LA12_0==45||LA12_0==52) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:243:12: (expr_1= expr ) ( ',' (expr_2= expr ) )*
                    {
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:243:12: (expr_1= expr )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:243:13: expr_1= expr
                    {
                    pushFollow(FOLLOW_expr_in_list1117);
                    expr_1=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              exprs.add((expr_1!=null?expr_1.st:null));
                          
                    }

                    }

                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:247:7: ( ',' (expr_2= expr ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==COMMA) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:247:9: ',' (expr_2= expr )
                    	    {
                    	    match(input,COMMA,FOLLOW_COMMA_in_list1133); if (state.failed) return retval;
                    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:247:13: (expr_2= expr )
                    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:247:14: expr_2= expr
                    	    {
                    	    pushFollow(FOLLOW_expr_in_list1140);
                    	    expr_2=expr();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {

                    	              exprs.add((expr_2!=null?expr_2.st:null));
                    	          
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }
                    break;

            }

            match(input,53,FOLLOW_53_in_list1160); if (state.failed) return retval;

            }



            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 252:5: -> list(exprs=exprs)
              {
                  retval.st = templateLib.getInstanceOf("list",
                new STAttrMap().put("exprs", exprs));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "list"

    public static class stmt_next_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_next"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:255:1: stmt_next : ID '.next' -> stmt_next();
    public final SimpleListsParser.stmt_next_return stmt_next() throws RecognitionException {
        SimpleListsParser.stmt_next_return retval = new SimpleListsParser.stmt_next_return();
        retval.start = input.LT(1);

        Token ID31=null;

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:256:5: ( ID '.next' -> stmt_next())
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:256:7: ID '.next'
            {
            ID31=(Token)match(input,ID,FOLLOW_ID_in_stmt_next1196); if (state.failed) return retval;
            match(input,54,FOLLOW_54_in_stmt_next1198); if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                      Function func = null;
                      for(Function f: ((program_scope)program_stack.peek()).functions.getFunctions())
                          if (f.getId().equals((ID31!=null?ID31.getText():null)))
                              func = f;
                      if (func == null) 
                          errors.add(new Error("line " + (ID31!=null?ID31.getLine():0) + " - invalid coroutine identifier: " + (ID31!=null?ID31.getText():null))); 
                      else if (func.getReturnValue() != FunReturn.YIELD)       
                          errors.add(new Error("line " + (ID31!=null?ID31.getLine():0) + " - not a coroutine identifier: " + (ID31!=null?ID31.getText():null))); 
                      else if (!((program_scope)program_stack.peek()).initCoroutines.contains(func.getId()))   
                          errors.add(new Error("line " + (ID31!=null?ID31.getLine():0) + " - coroutine hasn't been started: " + (ID31!=null?ID31.getText():null)));    
                  
            }


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 269:5: -> stmt_next()
              {
                  retval.st = templateLib.getInstanceOf("stmt_next");
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_next"

    public static class stmt_coroutine_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_coroutine"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:272:1: stmt_coroutine : ID '.begin' '(' call_params ')' -> stmt_coroutine(id=$ID.textcallParams=$call_params.callParams);
    public final SimpleListsParser.stmt_coroutine_return stmt_coroutine() throws RecognitionException {
        SimpleListsParser.stmt_coroutine_return retval = new SimpleListsParser.stmt_coroutine_return();
        retval.start = input.LT(1);

        Token ID32=null;
        SimpleListsParser.call_params_return call_params33 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:273:5: ( ID '.begin' '(' call_params ')' -> stmt_coroutine(id=$ID.textcallParams=$call_params.callParams))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:273:7: ID '.begin' '(' call_params ')'
            {
            ID32=(Token)match(input,ID,FOLLOW_ID_in_stmt_coroutine1231); if (state.failed) return retval;
            match(input,55,FOLLOW_55_in_stmt_coroutine1233); if (state.failed) return retval;
            match(input,45,FOLLOW_45_in_stmt_coroutine1235); if (state.failed) return retval;
            pushFollow(FOLLOW_call_params_in_stmt_coroutine1237);
            call_params33=call_params();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_stmt_coroutine1239); if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                      Function func = null;
                      for(Function f: ((program_scope)program_stack.peek()).functions.getFunctions())
                          if (f.getId().equals((ID32!=null?ID32.getText():null)))
                              func = f;
                      if (func == null) 
                          errors.add(new Error("line " + (ID32!=null?ID32.getLine():0) + " - invalid coroutine identifier: " + (ID32!=null?ID32.getText():null))); 
                      else if (func.getReturnValue() != FunReturn.YIELD)       
                          errors.add(new Error("line " + (ID32!=null?ID32.getLine():0) + " - not a coroutine identifier: " + (ID32!=null?ID32.getText():null))); 
                      else if (!((program_scope)program_stack.peek()).initCoroutines.contains(func.getId()))   
                              ((program_scope)program_stack.peek()).initCoroutines.add(func);            
                  
            }


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 286:5: -> stmt_coroutine(id=$ID.textcallParams=$call_params.callParams)
              {
                  retval.st = templateLib.getInstanceOf("stmt_coroutine",
                new STAttrMap().put("id", (ID32!=null?ID32.getText():null)).put("callParams", (call_params33!=null?call_params33.callParams:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_coroutine"

    protected static class stmt_func_scope {
        boolean isCor;
    }
    protected Stack stmt_func_stack = new Stack();

    public static class stmt_func_return extends ParserRuleReturnScope {
        public Function func;
        public String id;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_func"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:289:1: stmt_func returns [Function func, String id] : ID '(' call_params ')' -> stmt_func(id=$ID.textcallParams=$call_params.callParamsisCor=$stmt_func::isCor);
    public final SimpleListsParser.stmt_func_return stmt_func() throws RecognitionException {
        stmt_func_stack.push(new stmt_func_scope());
        SimpleListsParser.stmt_func_return retval = new SimpleListsParser.stmt_func_return();
        retval.start = input.LT(1);

        Token ID34=null;
        SimpleListsParser.call_params_return call_params35 = null;



            

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:298:5: ( ID '(' call_params ')' -> stmt_func(id=$ID.textcallParams=$call_params.callParamsisCor=$stmt_func::isCor))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:298:7: ID '(' call_params ')'
            {
            ID34=(Token)match(input,ID,FOLLOW_ID_in_stmt_func1297); if (state.failed) return retval;
            match(input,45,FOLLOW_45_in_stmt_func1299); if (state.failed) return retval;
            pushFollow(FOLLOW_call_params_in_stmt_func1301);
            call_params35=call_params();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_stmt_func1303); if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                      retval.func = null;
                      for(Function f: ((program_scope)program_stack.peek()).functions.getFunctions())
                          if (f.getId().equals((ID34!=null?ID34.getText():null)))
                              retval.func = f;
                      retval.id = (ID34!=null?ID34.getText():null);
                      int paramsNum;
                      if((call_params35!=null?call_params35.callParams:null) == null)
                          paramsNum = 0;
                      else paramsNum = (call_params35!=null?call_params35.callParams:null).size();
                      
                      
                      if(retval.func == null)
                          errors.add(new Error("line " + (ID34!=null?ID34.getLine():0) + " - invalid function identifier: " + retval.id));            
                      else if(paramsNum != retval.func.getParams().size())
                          errors.add(new Error("line " + (ID34!=null?ID34.getLine():0) + " - invalid params number: " + retval.func.getId()));
                      
                      if (retval.func.getReturnValue() == FunReturn.YIELD)    
                          ((stmt_func_scope)stmt_func_stack.peek()).isCor = true;   
                      else ((stmt_func_scope)stmt_func_stack.peek()).isCor = false;   
                  
            }


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 320:5: -> stmt_func(id=$ID.textcallParams=$call_params.callParamsisCor=$stmt_func::isCor)
              {
                  retval.st = templateLib.getInstanceOf("stmt_func",
                new STAttrMap().put("id", (ID34!=null?ID34.getText():null)).put("callParams", (call_params35!=null?call_params35.callParams:null)).put("isCor", ((stmt_func_scope)stmt_func_stack.peek()).isCor));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            stmt_func_stack.pop();
        }
        return retval;
    }
    // $ANTLR end "stmt_func"

    public static class stmt_if_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_if"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:323:1: stmt_if : 'if' '(' log_stmt ')' block[new ArrayList<String>()] ( stmt_else )? -> stmt_if(logStmt=$log_stmt.stblock=$block.stelseStmt=$stmt_else.st);
    public final SimpleListsParser.stmt_if_return stmt_if() throws RecognitionException {
        SimpleListsParser.stmt_if_return retval = new SimpleListsParser.stmt_if_return();
        retval.start = input.LT(1);

        SimpleListsParser.log_stmt_return log_stmt36 = null;

        SimpleListsParser.block_return block37 = null;

        SimpleListsParser.stmt_else_return stmt_else38 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:324:5: ( 'if' '(' log_stmt ')' block[new ArrayList<String>()] ( stmt_else )? -> stmt_if(logStmt=$log_stmt.stblock=$block.stelseStmt=$stmt_else.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:324:7: 'if' '(' log_stmt ')' block[new ArrayList<String>()] ( stmt_else )?
            {
            match(input,56,FOLLOW_56_in_stmt_if1358); if (state.failed) return retval;
            match(input,45,FOLLOW_45_in_stmt_if1360); if (state.failed) return retval;
            pushFollow(FOLLOW_log_stmt_in_stmt_if1362);
            log_stmt36=log_stmt();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_stmt_if1364); if (state.failed) return retval;
            pushFollow(FOLLOW_block_in_stmt_if1366);
            block37=block(new ArrayList<String>());

            state._fsp--;
            if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:324:60: ( stmt_else )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==57) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: stmt_else
                    {
                    pushFollow(FOLLOW_stmt_else_in_stmt_if1369);
                    stmt_else38=stmt_else();

                    state._fsp--;
                    if (state.failed) return retval;

                    }
                    break;

            }



            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 325:5: -> stmt_if(logStmt=$log_stmt.stblock=$block.stelseStmt=$stmt_else.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_if",
                new STAttrMap().put("logStmt", (log_stmt36!=null?log_stmt36.st:null)).put("block", (block37!=null?block37.st:null)).put("elseStmt", (stmt_else38!=null?stmt_else38.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_if"

    public static class stmt_else_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_else"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:328:1: stmt_else : 'else' block[new ArrayList<String>()] -> stmt_else(block=$block.st);
    public final SimpleListsParser.stmt_else_return stmt_else() throws RecognitionException {
        SimpleListsParser.stmt_else_return retval = new SimpleListsParser.stmt_else_return();
        retval.start = input.LT(1);

        SimpleListsParser.block_return block39 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:328:10: ( 'else' block[new ArrayList<String>()] -> stmt_else(block=$block.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:328:12: 'else' block[new ArrayList<String>()]
            {
            match(input,57,FOLLOW_57_in_stmt_else1414); if (state.failed) return retval;
            pushFollow(FOLLOW_block_in_stmt_else1416);
            block39=block(new ArrayList<String>());

            state._fsp--;
            if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 329:5: -> stmt_else(block=$block.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_else",
                new STAttrMap().put("block", (block39!=null?block39.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_else"

    public static class stmt_while_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_while"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:332:1: stmt_while : 'while' '(' log_stmt ')' block[new ArrayList<String>()] -> stmt_while(logStmt=$log_stmt.stblock=$block.st);
    public final SimpleListsParser.stmt_while_return stmt_while() throws RecognitionException {
        SimpleListsParser.stmt_while_return retval = new SimpleListsParser.stmt_while_return();
        retval.start = input.LT(1);

        SimpleListsParser.log_stmt_return log_stmt40 = null;

        SimpleListsParser.block_return block41 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:333:5: ( 'while' '(' log_stmt ')' block[new ArrayList<String>()] -> stmt_while(logStmt=$log_stmt.stblock=$block.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:333:7: 'while' '(' log_stmt ')' block[new ArrayList<String>()]
            {
            match(input,58,FOLLOW_58_in_stmt_while1448); if (state.failed) return retval;
            match(input,45,FOLLOW_45_in_stmt_while1450); if (state.failed) return retval;
            pushFollow(FOLLOW_log_stmt_in_stmt_while1452);
            log_stmt40=log_stmt();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_stmt_while1454); if (state.failed) return retval;
            pushFollow(FOLLOW_block_in_stmt_while1456);
            block41=block(new ArrayList<String>());

            state._fsp--;
            if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 334:5: -> stmt_while(logStmt=$log_stmt.stblock=$block.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_while",
                new STAttrMap().put("logStmt", (log_stmt40!=null?log_stmt40.st:null)).put("block", (block41!=null?block41.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_while"

    protected static class stmt_for_scope {
        List<String> assigned;
    }
    protected Stack stmt_for_stack = new Stack();

    public static class stmt_for_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_for"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:337:1: stmt_for : 'for' '(' for_stmts ')' block[$stmt_for::assigned] -> stmt_for(forStmts=$for_stmts.stblock=$block.st);
    public final SimpleListsParser.stmt_for_return stmt_for() throws RecognitionException {
        stmt_for_stack.push(new stmt_for_scope());
        SimpleListsParser.stmt_for_return retval = new SimpleListsParser.stmt_for_return();
        retval.start = input.LT(1);

        SimpleListsParser.for_stmts_return for_stmts42 = null;

        SimpleListsParser.block_return block43 = null;



            ((stmt_for_scope)stmt_for_stack.peek()).assigned = new ArrayList<String>();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:350:5: ( 'for' '(' for_stmts ')' block[$stmt_for::assigned] -> stmt_for(forStmts=$for_stmts.stblock=$block.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:350:7: 'for' '(' for_stmts ')' block[$stmt_for::assigned]
            {
            match(input,59,FOLLOW_59_in_stmt_for1510); if (state.failed) return retval;
            match(input,45,FOLLOW_45_in_stmt_for1512); if (state.failed) return retval;
            pushFollow(FOLLOW_for_stmts_in_stmt_for1514);
            for_stmts42=for_stmts();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_stmt_for1516); if (state.failed) return retval;
            pushFollow(FOLLOW_block_in_stmt_for1518);
            block43=block(((stmt_for_scope)stmt_for_stack.peek()).assigned);

            state._fsp--;
            if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 351:5: -> stmt_for(forStmts=$for_stmts.stblock=$block.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_for",
                new STAttrMap().put("forStmts", (for_stmts42!=null?for_stmts42.st:null)).put("block", (block43!=null?block43.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {
                  
                  ((program_scope)program_stack.peek()).blocks.delForVar();;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            stmt_for_stack.pop();
        }
        return retval;
    }
    // $ANTLR end "stmt_for"

    public static class for_stmts_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "for_stmts"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:354:1: for_stmts : (assign_1= for_assign[true] )? SEMI ( log_stmt )? SEMI (assign_2= for_assign[false] )? -> for_stmts(stmtAssign=$assign_1.stlogStmt=$log_stmt.ststmtAssign2=$assign_2.st);
    public final SimpleListsParser.for_stmts_return for_stmts() throws RecognitionException {
        SimpleListsParser.for_stmts_return retval = new SimpleListsParser.for_stmts_return();
        retval.start = input.LT(1);

        SimpleListsParser.for_assign_return assign_1 = null;

        SimpleListsParser.for_assign_return assign_2 = null;

        SimpleListsParser.log_stmt_return log_stmt44 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:355:5: ( (assign_1= for_assign[true] )? SEMI ( log_stmt )? SEMI (assign_2= for_assign[false] )? -> for_stmts(stmtAssign=$assign_1.stlogStmt=$log_stmt.ststmtAssign2=$assign_2.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:355:7: (assign_1= for_assign[true] )? SEMI ( log_stmt )? SEMI (assign_2= for_assign[false] )?
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:355:16: (assign_1= for_assign[true] )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==ID) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: assign_1= for_assign[true]
                    {
                    pushFollow(FOLLOW_for_assign_in_for_stmts1562);
                    assign_1=for_assign(true);

                    state._fsp--;
                    if (state.failed) return retval;

                    }
                    break;

            }

            match(input,SEMI,FOLLOW_SEMI_in_for_stmts1566); if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:355:41: ( log_stmt )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=ID && LA15_0<=INT)||LA15_0==STRING||LA15_0==45||LA15_0==52) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: log_stmt
                    {
                    pushFollow(FOLLOW_log_stmt_in_for_stmts1568);
                    log_stmt44=log_stmt();

                    state._fsp--;
                    if (state.failed) return retval;

                    }
                    break;

            }

            match(input,SEMI,FOLLOW_SEMI_in_for_stmts1571); if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:355:65: (assign_2= for_assign[false] )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: assign_2= for_assign[false]
                    {
                    pushFollow(FOLLOW_for_assign_in_for_stmts1577);
                    assign_2=for_assign(false);

                    state._fsp--;
                    if (state.failed) return retval;

                    }
                    break;

            }



            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 356:5: -> for_stmts(stmtAssign=$assign_1.stlogStmt=$log_stmt.ststmtAssign2=$assign_2.st)
              {
                  retval.st = templateLib.getInstanceOf("for_stmts",
                new STAttrMap().put("stmtAssign", (assign_1!=null?assign_1.st:null)).put("logStmt", (log_stmt44!=null?log_stmt44.st:null)).put("stmtAssign2", (assign_2!=null?assign_2.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "for_stmts"

    public static class for_assign_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "for_assign"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:359:1: for_assign[boolean isFirst] : (id= ID ) ASSIGN value -> for_assign(id=$id.textexpr=$value.stisFirst=$isFirst);
    public final SimpleListsParser.for_assign_return for_assign(boolean isFirst) throws RecognitionException {
        SimpleListsParser.for_assign_return retval = new SimpleListsParser.for_assign_return();
        retval.start = input.LT(1);

        Token id=null;
        SimpleListsParser.value_return value45 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:360:5: ( (id= ID ) ASSIGN value -> for_assign(id=$id.textexpr=$value.stisFirst=$isFirst))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:360:7: (id= ID ) ASSIGN value
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:360:7: (id= ID )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:360:8: id= ID
            {
            id=(Token)match(input,ID,FOLLOW_ID_in_for_assign1634); if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                      if(isFirst) {
                          if (((program_scope)program_stack.peek()).blocks.checkIdInBlocks((id!=null?id.getText():null)) || ((program_scope)program_stack.peek()).blocks.checkIdForVars((id!=null?id.getText():null))) 
                              isFirst = false;   
                          
                          ((program_scope)program_stack.peek()).blocks.addForVar((id!=null?id.getText():null));
                      }
                       
                  
            }

            }

            match(input,ASSIGN,FOLLOW_ASSIGN_in_for_assign1649); if (state.failed) return retval;
            pushFollow(FOLLOW_value_in_for_assign1651);
            value45=value();

            state._fsp--;
            if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 371:5: -> for_assign(id=$id.textexpr=$value.stisFirst=$isFirst)
              {
                  retval.st = templateLib.getInstanceOf("for_assign",
                new STAttrMap().put("id", (id!=null?id.getText():null)).put("expr", (value45!=null?value45.st:null)).put("isFirst", isFirst));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "for_assign"

    public static class stmt_for_list_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_for_list"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:374:1: stmt_for_list : 'foreach' '(' ex_1= ID 'in' ex_2= expr ')' block[new ArrayList()] -> stmt_for_list(ex_1=$ex_1.textex_2=$ex_2.stblock=$block.st);
    public final SimpleListsParser.stmt_for_list_return stmt_for_list() throws RecognitionException {
        SimpleListsParser.stmt_for_list_return retval = new SimpleListsParser.stmt_for_list_return();
        retval.start = input.LT(1);

        Token ex_1=null;
        SimpleListsParser.expr_return ex_2 = null;

        SimpleListsParser.block_return block46 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:375:5: ( 'foreach' '(' ex_1= ID 'in' ex_2= expr ')' block[new ArrayList()] -> stmt_for_list(ex_1=$ex_1.textex_2=$ex_2.stblock=$block.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:375:7: 'foreach' '(' ex_1= ID 'in' ex_2= expr ')' block[new ArrayList()]
            {
            match(input,60,FOLLOW_60_in_stmt_for_list1700); if (state.failed) return retval;
            match(input,45,FOLLOW_45_in_stmt_for_list1702); if (state.failed) return retval;
            ex_1=(Token)match(input,ID,FOLLOW_ID_in_stmt_for_list1708); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
              ((program_scope)program_stack.peek()).blocks.addForVar((ex_1!=null?ex_1.getText():null));
            }
            match(input,61,FOLLOW_61_in_stmt_for_list1712); if (state.failed) return retval;
            pushFollow(FOLLOW_expr_in_stmt_for_list1718);
            ex_2=expr();

            state._fsp--;
            if (state.failed) return retval;
            match(input,46,FOLLOW_46_in_stmt_for_list1720); if (state.failed) return retval;
            pushFollow(FOLLOW_block_in_stmt_for_list1722);
            block46=block(new ArrayList());

            state._fsp--;
            if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 376:5: -> stmt_for_list(ex_1=$ex_1.textex_2=$ex_2.stblock=$block.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_for_list",
                new STAttrMap().put("ex_1", (ex_1!=null?ex_1.getText():null)).put("ex_2", (ex_2!=null?ex_2.st:null)).put("block", (block46!=null?block46.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_for_list"

    public static class stmt_yield_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_yield"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:379:1: stmt_yield : 'yield' expr -> stmt_yield(expr=$expr.st);
    public final SimpleListsParser.stmt_yield_return stmt_yield() throws RecognitionException {
        SimpleListsParser.stmt_yield_return retval = new SimpleListsParser.stmt_yield_return();
        retval.start = input.LT(1);

        SimpleListsParser.expr_return expr47 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:380:5: ( 'yield' expr -> stmt_yield(expr=$expr.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:380:7: 'yield' expr
            {
            match(input,62,FOLLOW_62_in_stmt_yield1772); if (state.failed) return retval;
            pushFollow(FOLLOW_expr_in_stmt_yield1774);
            expr47=expr();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                      ((function_scope)function_stack.peek()).returnType = FunReturn.YIELD;
                      ((function_scope)function_stack.peek()).isCor = true;
                  
            }


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 385:5: -> stmt_yield(expr=$expr.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_yield",
                new STAttrMap().put("expr", (expr47!=null?expr47.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_yield"

    public static class stmt_return_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_return"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:388:1: stmt_return : 'return' expr -> stmt_return(expr=$expr.st);
    public final SimpleListsParser.stmt_return_return stmt_return() throws RecognitionException {
        SimpleListsParser.stmt_return_return retval = new SimpleListsParser.stmt_return_return();
        retval.start = input.LT(1);

        SimpleListsParser.expr_return expr48 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:389:5: ( 'return' expr -> stmt_return(expr=$expr.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:389:7: 'return' expr
            {
            match(input,63,FOLLOW_63_in_stmt_return1820); if (state.failed) return retval;
            pushFollow(FOLLOW_expr_in_stmt_return1822);
            expr48=expr();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) {

                      ((function_scope)function_stack.peek()).returnType = FunReturn.RETURN;
                  
            }


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 393:5: -> stmt_return(expr=$expr.st)
              {
                  retval.st = templateLib.getInstanceOf("stmt_return",
                new STAttrMap().put("expr", (expr48!=null?expr48.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_return"

    public static class stmt_break_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmt_break"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:396:1: stmt_break : 'break' -> stmt_break();
    public final SimpleListsParser.stmt_break_return stmt_break() throws RecognitionException {
        SimpleListsParser.stmt_break_return retval = new SimpleListsParser.stmt_break_return();
        retval.start = input.LT(1);

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:397:5: ( 'break' -> stmt_break())
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:397:7: 'break'
            {
            match(input,64,FOLLOW_64_in_stmt_break1868); if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 398:5: -> stmt_break()
              {
                  retval.st = templateLib.getInstanceOf("stmt_break");
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmt_break"

    public static class call_params_return extends ParserRuleReturnScope {
        public List<String> callParams;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "call_params"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:401:1: call_params returns [List<String> callParams] : (paramsLabel+= expr ( ',' paramsLabel+= expr )* )? ;
    public final SimpleListsParser.call_params_return call_params() throws RecognitionException {
        SimpleListsParser.call_params_return retval = new SimpleListsParser.call_params_return();
        retval.start = input.LT(1);

        List list_paramsLabel=null;
        RuleReturnScope paramsLabel = null;
        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:402:5: ( (paramsLabel+= expr ( ',' paramsLabel+= expr )* )? )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:402:7: (paramsLabel+= expr ( ',' paramsLabel+= expr )* )?
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:402:7: (paramsLabel+= expr ( ',' paramsLabel+= expr )* )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( ((LA18_0>=ID && LA18_0<=INT)||LA18_0==STRING||LA18_0==45||LA18_0==52) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:402:9: paramsLabel+= expr ( ',' paramsLabel+= expr )*
                    {
                    pushFollow(FOLLOW_expr_in_call_params1905);
                    paramsLabel=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if (list_paramsLabel==null) list_paramsLabel=new ArrayList();
                    list_paramsLabel.add(paramsLabel.getTemplate());

                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:402:27: ( ',' paramsLabel+= expr )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==COMMA) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:402:29: ',' paramsLabel+= expr
                    	    {
                    	    match(input,COMMA,FOLLOW_COMMA_in_call_params1909); if (state.failed) return retval;
                    	    pushFollow(FOLLOW_expr_in_call_params1913);
                    	    paramsLabel=expr();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if (list_paramsLabel==null) list_paramsLabel=new ArrayList();
                    	    list_paramsLabel.add(paramsLabel.getTemplate());


                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);


                    }
                    break;

            }

            if ( state.backtracking==0 ) {
              retval.callParams = list_paramsLabel;
            }

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "call_params"

    public static class stmts_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "stmts"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:405:1: stmts : ( ( NEWLINE )* stmt )* ;
    public final SimpleListsParser.stmts_return stmts() throws RecognitionException {
        SimpleListsParser.stmts_return retval = new SimpleListsParser.stmts_return();
        retval.start = input.LT(1);

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:406:5: ( ( ( NEWLINE )* stmt )* )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:406:7: ( ( NEWLINE )* stmt )*
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:406:7: ( ( NEWLINE )* stmt )*
            loop20:
            do {
                int alt20=2;
                alt20 = dfa20.predict(input);
                switch (alt20) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:406:8: ( NEWLINE )* stmt
            	    {
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:406:8: ( NEWLINE )*
            	    loop19:
            	    do {
            	        int alt19=2;
            	        int LA19_0 = input.LA(1);

            	        if ( (LA19_0==NEWLINE) ) {
            	            alt19=1;
            	        }


            	        switch (alt19) {
            	    	case 1 :
            	    	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: NEWLINE
            	    	    {
            	    	    match(input,NEWLINE,FOLLOW_NEWLINE_in_stmts1943); if (state.failed) return retval;

            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop19;
            	        }
            	    } while (true);

            	    pushFollow(FOLLOW_stmt_in_stmts1946);
            	    stmt();

            	    state._fsp--;
            	    if (state.failed) return retval;

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "stmts"

    public static class log_stmt_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "log_stmt"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:409:1: log_stmt : expr_1= expr ( ( LOGOP expr_2= expr ) -> log_stmt(expr_1=$expr_1.stexpr_2=$expr_2.stlogOp=logOp) | ( is_expr[$expr_1.st] ) -> {$is_expr.st}) ;
    public final SimpleListsParser.log_stmt_return log_stmt() throws RecognitionException {
        SimpleListsParser.log_stmt_return retval = new SimpleListsParser.log_stmt_return();
        retval.start = input.LT(1);

        Token LOGOP49=null;
        SimpleListsParser.expr_return expr_1 = null;

        SimpleListsParser.expr_return expr_2 = null;

        SimpleListsParser.is_expr_return is_expr50 = null;



            String logOp = new String();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:414:5: (expr_1= expr ( ( LOGOP expr_2= expr ) -> log_stmt(expr_1=$expr_1.stexpr_2=$expr_2.stlogOp=logOp) | ( is_expr[$expr_1.st] ) -> {$is_expr.st}) )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:414:7: expr_1= expr ( ( LOGOP expr_2= expr ) -> log_stmt(expr_1=$expr_1.stexpr_2=$expr_2.stlogOp=logOp) | ( is_expr[$expr_1.st] ) -> {$is_expr.st})
            {
            pushFollow(FOLLOW_expr_in_log_stmt1977);
            expr_1=expr();

            state._fsp--;
            if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:414:21: ( ( LOGOP expr_2= expr ) -> log_stmt(expr_1=$expr_1.stexpr_2=$expr_2.stlogOp=logOp) | ( is_expr[$expr_1.st] ) -> {$is_expr.st})
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==LOGOP) ) {
                alt21=1;
            }
            else if ( (LA21_0==DOT) ) {
                alt21=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:414:22: ( LOGOP expr_2= expr )
                    {
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:414:22: ( LOGOP expr_2= expr )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:414:23: LOGOP expr_2= expr
                    {
                    LOGOP49=(Token)match(input,LOGOP,FOLLOW_LOGOP_in_log_stmt1981); if (state.failed) return retval;
                    pushFollow(FOLLOW_expr_in_log_stmt1987);
                    expr_2=expr();

                    state._fsp--;
                    if (state.failed) return retval;

                    }

                    if ( state.backtracking==0 ) {

                              if((LOGOP49!=null?LOGOP49.getText():null).equals("=="))
                                  logOp = "equals";
                              else if((LOGOP49!=null?LOGOP49.getText():null).equals("<"))
                                  logOp = "less";   
                              else if((LOGOP49!=null?LOGOP49.getText():null).equals(">"))
                                  logOp = "greater";  
                              else if((LOGOP49!=null?LOGOP49.getText():null).equals("!="))
                                  logOp = "notEquals";           
                          
                    }


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 425:5: -> log_stmt(expr_1=$expr_1.stexpr_2=$expr_2.stlogOp=logOp)
                      {
                          retval.st = templateLib.getInstanceOf("log_stmt",
                        new STAttrMap().put("expr_1", (expr_1!=null?expr_1.st:null)).put("expr_2", (expr_2!=null?expr_2.st:null)).put("logOp", logOp));
                      }

                    }
                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:426:5: ( is_expr[$expr_1.st] )
                    {
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:426:5: ( is_expr[$expr_1.st] )
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:426:6: is_expr[$expr_1.st]
                    {
                    pushFollow(FOLLOW_is_expr_in_log_stmt2034);
                    is_expr50=is_expr((expr_1!=null?expr_1.st:null));

                    state._fsp--;
                    if (state.failed) return retval;

                    }



                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 427:5: -> {$is_expr.st}
                      {
                          retval.st = (is_expr50!=null?is_expr50.st:null);
                      }

                    }
                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "log_stmt"

    public static class is_expr_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "is_expr"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:431:1: is_expr[StringTemplate expr] : '.' ( ISINT -> isInt(expr=$expr) | ISSTRING -> isString(expr=$expr) | ISLIST -> isList(expr=$expr)) ;
    public final SimpleListsParser.is_expr_return is_expr(StringTemplate expr) throws RecognitionException {
        SimpleListsParser.is_expr_return retval = new SimpleListsParser.is_expr_return();
        retval.start = input.LT(1);

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:432:2: ( '.' ( ISINT -> isInt(expr=$expr) | ISSTRING -> isString(expr=$expr) | ISLIST -> isList(expr=$expr)) )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:432:4: '.' ( ISINT -> isInt(expr=$expr) | ISSTRING -> isString(expr=$expr) | ISLIST -> isList(expr=$expr))
            {
            match(input,DOT,FOLLOW_DOT_in_is_expr2080); if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:432:8: ( ISINT -> isInt(expr=$expr) | ISSTRING -> isString(expr=$expr) | ISLIST -> isList(expr=$expr))
            int alt22=3;
            switch ( input.LA(1) ) {
            case ISINT:
                {
                alt22=1;
                }
                break;
            case ISSTRING:
                {
                alt22=2;
                }
                break;
            case ISLIST:
                {
                alt22=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:432:9: ISINT
                    {
                    match(input,ISINT,FOLLOW_ISINT_in_is_expr2083); if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 433:2: -> isInt(expr=$expr)
                      {
                          retval.st = templateLib.getInstanceOf("isInt",
                        new STAttrMap().put("expr", expr));
                      }

                    }
                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:434:2: ISSTRING
                    {
                    match(input,ISSTRING,FOLLOW_ISSTRING_in_is_expr2102); if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 435:2: -> isString(expr=$expr)
                      {
                          retval.st = templateLib.getInstanceOf("isString",
                        new STAttrMap().put("expr", expr));
                      }

                    }
                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:436:2: ISLIST
                    {
                    match(input,ISLIST,FOLLOW_ISLIST_in_is_expr2119); if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 437:2: -> isList(expr=$expr)
                      {
                          retval.st = templateLib.getInstanceOf("isList",
                        new STAttrMap().put("expr", expr));
                      }

                    }
                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "is_expr"

    public static class expr_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "expr"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:441:1: expr : mult= mult_expr ( ( '+' | '-' ) mult_2= mult_expr )* -> expr(startExpr=$mult.stmultExprs=multExprs);
    public final SimpleListsParser.expr_return expr() throws RecognitionException {
        SimpleListsParser.expr_return retval = new SimpleListsParser.expr_return();
        retval.start = input.LT(1);

        SimpleListsParser.mult_expr_return mult = null;

        SimpleListsParser.mult_expr_return mult_2 = null;



            List multExprs = new ArrayList();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:446:5: (mult= mult_expr ( ( '+' | '-' ) mult_2= mult_expr )* -> expr(startExpr=$mult.stmultExprs=multExprs))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:446:7: mult= mult_expr ( ( '+' | '-' ) mult_2= mult_expr )*
            {
            pushFollow(FOLLOW_mult_expr_in_expr2160);
            mult=mult_expr();

            state._fsp--;
            if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:446:24: ( ( '+' | '-' ) mult_2= mult_expr )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( ((LA24_0>=PLUS && LA24_0<=MINUS)) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:446:25: ( '+' | '-' ) mult_2= mult_expr
            	    {
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:446:25: ( '+' | '-' )
            	    int alt23=2;
            	    int LA23_0 = input.LA(1);

            	    if ( (LA23_0==PLUS) ) {
            	        alt23=1;
            	    }
            	    else if ( (LA23_0==MINUS) ) {
            	        alt23=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 23, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt23) {
            	        case 1 :
            	            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:446:26: '+'
            	            {
            	            match(input,PLUS,FOLLOW_PLUS_in_expr2164); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	              multExprs.add(".add(");
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:446:56: '-'
            	            {
            	            match(input,MINUS,FOLLOW_MINUS_in_expr2168); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	              multExprs.add(".sub(");
            	            }

            	            }
            	            break;

            	    }

            	    pushFollow(FOLLOW_mult_expr_in_expr2176);
            	    mult_2=mult_expr();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {

            	              multExprs.add((mult_2!=null?mult_2.st:null).toString());
            	              multExprs.add(")");
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);



            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 452:5: -> expr(startExpr=$mult.stmultExprs=multExprs)
              {
                  retval.st = templateLib.getInstanceOf("expr",
                new STAttrMap().put("startExpr", (mult!=null?mult.st:null)).put("multExprs", multExprs));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "expr"

    public static class mult_expr_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "mult_expr"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:455:1: mult_expr : atom_1= atom ( ( '*' | '/' ) atom_2= atom )* -> mult_expr(startAtom=$atom_1.statoms=atoms);
    public final SimpleListsParser.mult_expr_return mult_expr() throws RecognitionException {
        SimpleListsParser.mult_expr_return retval = new SimpleListsParser.mult_expr_return();
        retval.start = input.LT(1);

        SimpleListsParser.atom_return atom_1 = null;

        SimpleListsParser.atom_return atom_2 = null;



            List atoms = new ArrayList();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:460:5: (atom_1= atom ( ( '*' | '/' ) atom_2= atom )* -> mult_expr(startAtom=$atom_1.statoms=atoms))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:460:7: atom_1= atom ( ( '*' | '/' ) atom_2= atom )*
            {
            pushFollow(FOLLOW_atom_in_mult_expr2242);
            atom_1=atom();

            state._fsp--;
            if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:461:5: ( ( '*' | '/' ) atom_2= atom )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=STAR && LA26_0<=SLASH)) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:461:6: ( '*' | '/' ) atom_2= atom
            	    {
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:461:6: ( '*' | '/' )
            	    int alt25=2;
            	    int LA25_0 = input.LA(1);

            	    if ( (LA25_0==STAR) ) {
            	        alt25=1;
            	    }
            	    else if ( (LA25_0==SLASH) ) {
            	        alt25=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 25, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt25) {
            	        case 1 :
            	            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:461:7: '*'
            	            {
            	            match(input,STAR,FOLLOW_STAR_in_mult_expr2251); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	              atoms.add(".mult(");
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:461:34: '/'
            	            {
            	            match(input,SLASH,FOLLOW_SLASH_in_mult_expr2255); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	              atoms.add(".div(");
            	            }

            	            }
            	            break;

            	    }

            	    pushFollow(FOLLOW_atom_in_mult_expr2264);
            	    atom_2=atom();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {

            	              atoms.add((atom_2!=null?atom_2.st:null).toString());
            	              atoms.add(")");
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);



            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 467:5: -> mult_expr(startAtom=$atom_1.statoms=atoms)
              {
                  retval.st = templateLib.getInstanceOf("mult_expr",
                new STAttrMap().put("startAtom", (atom_1!=null?atom_1.st:null)).put("atoms", atoms));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "mult_expr"

    public static class atom_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "atom"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:470:1: atom : ( ID | stmt_func | element | list | '(' expr ')' | stmt_next ) ( index )? ( point_expr )? -> atom(text=forTemplateindex=$index.stpoint_expr=$point_expr.st);
    public final SimpleListsParser.atom_return atom() throws RecognitionException {
        SimpleListsParser.atom_return retval = new SimpleListsParser.atom_return();
        retval.start = input.LT(1);

        Token ID51=null;
        SimpleListsParser.stmt_func_return stmt_func52 = null;

        SimpleListsParser.element_return element53 = null;

        SimpleListsParser.list_return list54 = null;

        SimpleListsParser.expr_return expr55 = null;

        SimpleListsParser.stmt_next_return stmt_next56 = null;

        SimpleListsParser.index_return index57 = null;

        SimpleListsParser.point_expr_return point_expr58 = null;



            String forTemplate = new String();

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:475:5: ( ( ID | stmt_func | element | list | '(' expr ')' | stmt_next ) ( index )? ( point_expr )? -> atom(text=forTemplateindex=$index.stpoint_expr=$point_expr.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:475:7: ( ID | stmt_func | element | list | '(' expr ')' | stmt_next ) ( index )? ( point_expr )?
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:475:7: ( ID | stmt_func | element | list | '(' expr ')' | stmt_next )
            int alt27=6;
            switch ( input.LA(1) ) {
            case ID:
                {
                switch ( input.LA(2) ) {
                case 45:
                    {
                    alt27=2;
                    }
                    break;
                case 54:
                    {
                    alt27=6;
                    }
                    break;
                case EOF:
                case DEDENT:
                case NEWLINE:
                case ID:
                case SEMI:
                case LOGOP:
                case COMMA:
                case PLUS:
                case MINUS:
                case STAR:
                case SLASH:
                case DOT:
                case 46:
                case 48:
                case 50:
                case 51:
                case 52:
                case 53:
                case 56:
                case 58:
                case 59:
                case 60:
                case 62:
                case 63:
                case 64:
                    {
                    alt27=1;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 27, 1, input);

                    throw nvae;
                }

                }
                break;
            case INT:
            case STRING:
                {
                alt27=3;
                }
                break;
            case 52:
                {
                alt27=4;
                }
                break;
            case 45:
                {
                alt27=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:475:8: ID
                    {
                    ID51=(Token)match(input,ID,FOLLOW_ID_in_atom2326); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              if(!((program_scope)program_stack.peek()).blocks.checkIdInBlocks((ID51!=null?ID51.getText():null)) && !((program_scope)program_stack.peek()).blocks.checkIdForVars((ID51!=null?ID51.getText():null)))
                                  errors.add(new Error("line " + (ID51!=null?ID51.getLine():0) + " - undefined variable in expression: " + (ID51!=null?ID51.getText():null)));
                              forTemplate = (ID51!=null?ID51.getText():null);    
                          
                    }

                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:481:5: stmt_func
                    {
                    pushFollow(FOLLOW_stmt_func_in_atom2342);
                    stmt_func52=stmt_func();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              if ((stmt_func52!=null?stmt_func52.func:null) != null && (stmt_func52!=null?stmt_func52.func:null).getReturnValue() == FunReturn.NONE)
                                  errors.add(new Error("line " + (ID51!=null?ID51.getLine():0) + " - function used in the expression does not return value: " 
                                  + (stmt_func52!=null?stmt_func52.func:null).getId()));
                              forTemplate = (stmt_func52!=null?stmt_func52.st:null).toString();    
                          
                    }

                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:488:5: element
                    {
                    pushFollow(FOLLOW_element_in_atom2358);
                    element53=element();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              forTemplate = "(new Type(" + (element53!=null?input.toString(element53.start,element53.stop):null) + "))";
                          
                    }

                    }
                    break;
                case 4 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:492:5: list
                    {
                    pushFollow(FOLLOW_list_in_atom2374);
                    list54=list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              forTemplate = (list54!=null?list54.st:null).toString();
                          
                    }

                    }
                    break;
                case 5 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:496:5: '(' expr ')'
                    {
                    match(input,45,FOLLOW_45_in_atom2389); if (state.failed) return retval;
                    pushFollow(FOLLOW_expr_in_atom2391);
                    expr55=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    match(input,46,FOLLOW_46_in_atom2393); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              forTemplate = '(' + (expr55!=null?expr55.st:null).toString() + ')';
                          
                    }

                    }
                    break;
                case 6 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:500:5: stmt_next
                    {
                    pushFollow(FOLLOW_stmt_next_in_atom2408);
                    stmt_next56=stmt_next();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) {

                              forTemplate = (stmt_next56!=null?stmt_next56.st:null).toString();
                          
                    }

                    }
                    break;

            }

            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:505:5: ( index )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==52) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: index
                    {
                    pushFollow(FOLLOW_index_in_atom2428);
                    index57=index();

                    state._fsp--;
                    if (state.failed) return retval;

                    }
                    break;

            }

            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:506:5: ( point_expr )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==DOT) ) {
                int LA29_1 = input.LA(2);

                if ( ((LA29_1>=TOINT && LA29_1<=ADD)) ) {
                    alt29=1;
                }
            }
            switch (alt29) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: point_expr
                    {
                    pushFollow(FOLLOW_point_expr_in_atom2436);
                    point_expr58=point_expr();

                    state._fsp--;
                    if (state.failed) return retval;

                    }
                    break;

            }



            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 507:5: -> atom(text=forTemplateindex=$index.stpoint_expr=$point_expr.st)
              {
                  retval.st = templateLib.getInstanceOf("atom",
                new STAttrMap().put("text", forTemplate).put("index", (index57!=null?index57.st:null)).put("point_expr", (point_expr58!=null?point_expr58.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "atom"

    public static class point_expr_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "point_expr"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:510:1: point_expr : ( '.' ( TOINT -> to_int() | TOSTRING -> to_string() | LENGTH -> length() | list_ops -> {$list_ops.st}) ) ;
    public final SimpleListsParser.point_expr_return point_expr() throws RecognitionException {
        SimpleListsParser.point_expr_return retval = new SimpleListsParser.point_expr_return();
        retval.start = input.LT(1);

        SimpleListsParser.list_ops_return list_ops59 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:511:5: ( ( '.' ( TOINT -> to_int() | TOSTRING -> to_string() | LENGTH -> length() | list_ops -> {$list_ops.st}) ) )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:511:7: ( '.' ( TOINT -> to_int() | TOSTRING -> to_string() | LENGTH -> length() | list_ops -> {$list_ops.st}) )
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:511:7: ( '.' ( TOINT -> to_int() | TOSTRING -> to_string() | LENGTH -> length() | list_ops -> {$list_ops.st}) )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:511:8: '.' ( TOINT -> to_int() | TOSTRING -> to_string() | LENGTH -> length() | list_ops -> {$list_ops.st})
            {
            match(input,DOT,FOLLOW_DOT_in_point_expr2487); if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:511:12: ( TOINT -> to_int() | TOSTRING -> to_string() | LENGTH -> length() | list_ops -> {$list_ops.st})
            int alt30=4;
            switch ( input.LA(1) ) {
            case TOINT:
                {
                alt30=1;
                }
                break;
            case TOSTRING:
                {
                alt30=2;
                }
                break;
            case LENGTH:
                {
                alt30=3;
                }
                break;
            case DEL:
            case ADD:
                {
                alt30=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:511:13: TOINT
                    {
                    match(input,TOINT,FOLLOW_TOINT_in_point_expr2490); if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 511:19: -> to_int()
                      {
                          retval.st = templateLib.getInstanceOf("to_int");
                      }

                    }
                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:512:13: TOSTRING
                    {
                    match(input,TOSTRING,FOLLOW_TOSTRING_in_point_expr2512); if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 512:22: -> to_string()
                      {
                          retval.st = templateLib.getInstanceOf("to_string");
                      }

                    }
                    }
                    break;
                case 3 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:513:13: LENGTH
                    {
                    match(input,LENGTH,FOLLOW_LENGTH_in_point_expr2533); if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 513:20: -> length()
                      {
                          retval.st = templateLib.getInstanceOf("length");
                      }

                    }
                    }
                    break;
                case 4 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:514:13: list_ops
                    {
                    pushFollow(FOLLOW_list_ops_in_point_expr2554);
                    list_ops59=list_ops();

                    state._fsp--;
                    if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 514:22: -> {$list_ops.st}
                      {
                          retval.st = (list_ops59!=null?list_ops59.st:null);
                      }

                    }
                    }
                    break;

            }


            }


            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "point_expr"

    public static class list_ops_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "list_ops"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:518:1: list_ops : ( DEL '(' expr ')' -> del(expr=$expr.st) | ADD '(' expr ')' -> add(expr=$expr.st));
    public final SimpleListsParser.list_ops_return list_ops() throws RecognitionException {
        SimpleListsParser.list_ops_return retval = new SimpleListsParser.list_ops_return();
        retval.start = input.LT(1);

        SimpleListsParser.expr_return expr60 = null;

        SimpleListsParser.expr_return expr61 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:519:5: ( DEL '(' expr ')' -> del(expr=$expr.st) | ADD '(' expr ')' -> add(expr=$expr.st))
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==DEL) ) {
                alt31=1;
            }
            else if ( (LA31_0==ADD) ) {
                alt31=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:519:7: DEL '(' expr ')'
                    {
                    match(input,DEL,FOLLOW_DEL_in_list_ops2583); if (state.failed) return retval;
                    match(input,45,FOLLOW_45_in_list_ops2585); if (state.failed) return retval;
                    pushFollow(FOLLOW_expr_in_list_ops2587);
                    expr60=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    match(input,46,FOLLOW_46_in_list_ops2589); if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 519:24: -> del(expr=$expr.st)
                      {
                          retval.st = templateLib.getInstanceOf("del",
                        new STAttrMap().put("expr", (expr60!=null?expr60.st:null)));
                      }

                    }
                    }
                    break;
                case 2 :
                    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:519:51: ADD '(' expr ')'
                    {
                    match(input,ADD,FOLLOW_ADD_in_list_ops2603); if (state.failed) return retval;
                    match(input,45,FOLLOW_45_in_list_ops2605); if (state.failed) return retval;
                    pushFollow(FOLLOW_expr_in_list_ops2607);
                    expr61=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    match(input,46,FOLLOW_46_in_list_ops2609); if (state.failed) return retval;


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 519:68: -> add(expr=$expr.st)
                      {
                          retval.st = templateLib.getInstanceOf("add",
                        new STAttrMap().put("expr", (expr61!=null?expr61.st:null)));
                      }

                    }
                    }
                    break;

            }
            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "list_ops"

    public static class index_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "index"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:522:1: index : ( '[' expr ']' ) -> index(expr=$expr.st);
    public final SimpleListsParser.index_return index() throws RecognitionException {
        SimpleListsParser.index_return retval = new SimpleListsParser.index_return();
        retval.start = input.LT(1);

        SimpleListsParser.expr_return expr62 = null;


        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:523:5: ( ( '[' expr ']' ) -> index(expr=$expr.st))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:523:7: ( '[' expr ']' )
            {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:523:7: ( '[' expr ']' )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:523:8: '[' expr ']'
            {
            match(input,52,FOLLOW_52_in_index2637); if (state.failed) return retval;
            pushFollow(FOLLOW_expr_in_index2639);
            expr62=expr();

            state._fsp--;
            if (state.failed) return retval;
            match(input,53,FOLLOW_53_in_index2641); if (state.failed) return retval;

            }



            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 524:5: -> index(expr=$expr.st)
              {
                  retval.st = templateLib.getInstanceOf("index",
                new STAttrMap().put("expr", (expr62!=null?expr62.st:null)));
              }

            }
            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "index"

    protected static class block_scope {
        List stmtsTempl;
    }
    protected Stack block_stack = new Stack();

    public static class block_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "block"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:527:1: block[List<String> params] : INDENT stmts ( NEWLINE )* DEDENT -> block(stmts=$block::stmtsTempl);
    public final SimpleListsParser.block_return block(List<String> params) throws RecognitionException {
        block_stack.push(new block_scope());
        SimpleListsParser.block_return retval = new SimpleListsParser.block_return();
        retval.start = input.LT(1);


            ((block_scope)block_stack.peek()).stmtsTempl = new ArrayList();
            Block block = new Block();
            for(String par: params)
                block.addVar(par);
            ((program_scope)program_stack.peek()).blocks.addBlock(block);
                

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:545:3: ( INDENT stmts ( NEWLINE )* DEDENT -> block(stmts=$block::stmtsTempl))
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:546:4: INDENT stmts ( NEWLINE )* DEDENT
            {
            match(input,INDENT,FOLLOW_INDENT_in_block2712); if (state.failed) return retval;
            pushFollow(FOLLOW_stmts_in_block2714);
            stmts();

            state._fsp--;
            if (state.failed) return retval;
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:546:17: ( NEWLINE )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==NEWLINE) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:0:0: NEWLINE
            	    {
            	    match(input,NEWLINE,FOLLOW_NEWLINE_in_block2716); if (state.failed) return retval;

            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

            match(input,DEDENT,FOLLOW_DEDENT_in_block2719); if (state.failed) return retval;


            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 547:4: -> block(stmts=$block::stmtsTempl)
              {
                  retval.st = templateLib.getInstanceOf("block",
                new STAttrMap().put("stmts", ((block_scope)block_stack.peek()).stmtsTempl));
              }

            }
            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

                  ((program_scope)program_stack.peek()).blocks.pop();

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            block_stack.pop();
        }
        return retval;
    }
    // $ANTLR end "block"

    public static class element_return extends ParserRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "element"
    // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:550:1: element : ( INT | STRING );
    public final SimpleListsParser.element_return element() throws RecognitionException {
        SimpleListsParser.element_return retval = new SimpleListsParser.element_return();
        retval.start = input.LT(1);

        try {
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:551:5: ( INT | STRING )
            // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:
            {
            if ( input.LA(1)==INT||input.LA(1)==STRING ) {
                input.consume();
                state.errorRecovery=false;state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "element"

    // $ANTLR start synpred22_SimpleLists
    public final void synpred22_SimpleLists_fragment() throws RecognitionException {   
        // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:232:7: ( expr )
        // D:\\tools\\eclipse-workspace\\SimpleLists\\src\\SimpleLists.g:232:7: expr
        {
        pushFollow(FOLLOW_expr_in_synpred22_SimpleLists1057);
        expr();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred22_SimpleLists

    // Delegated rules

    public final boolean synpred22_SimpleLists() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred22_SimpleLists_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA3 dfa3 = new DFA3(this);
    protected DFA6 dfa6 = new DFA6(this);
    protected DFA20 dfa20 = new DFA20(this);
    static final String DFA3_eotS =
        "\4\uffff";
    static final String DFA3_eofS =
        "\4\uffff";
    static final String DFA3_minS =
        "\2\6\2\uffff";
    static final String DFA3_maxS =
        "\2\57\2\uffff";
    static final String DFA3_acceptS =
        "\2\uffff\1\2\1\1";
    static final String DFA3_specialS =
        "\4\uffff}>";
    static final String[] DFA3_transitionS = {
            "\1\1\45\uffff\1\3\2\uffff\1\2",
            "\1\1\45\uffff\1\3\2\uffff\1\2",
            "",
            ""
    };

    static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
    static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
    static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
    static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
    static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
    static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
    static final short[][] DFA3_transition;

    static {
        int numStates = DFA3_transitionS.length;
        DFA3_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
        }
    }

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = DFA3_eot;
            this.eof = DFA3_eof;
            this.min = DFA3_min;
            this.max = DFA3_max;
            this.accept = DFA3_accept;
            this.special = DFA3_special;
            this.transition = DFA3_transition;
        }
        public String getDescription() {
            return "()* loopback of 77:8: ( ( NEWLINE )* function )*";
        }
    }
    static final String DFA6_eotS =
        "\20\uffff";
    static final String DFA6_eofS =
        "\20\uffff";
    static final String DFA6_minS =
        "\1\7\1\11\16\uffff";
    static final String DFA6_maxS =
        "\1\100\1\67\16\uffff";
    static final String DFA6_acceptS =
        "\2\uffff\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1\14\1\1\1\2\1"+
        "\15\1\16";
    static final String DFA6_specialS =
        "\20\uffff}>";
    static final String[] DFA6_transitionS = {
            "\1\1\50\uffff\1\13\1\uffff\1\12\1\6\4\uffff\1\5\1\uffff\1\7"+
            "\1\10\1\11\1\uffff\1\2\1\3\1\4",
            "\1\14\43\uffff\1\15\6\uffff\1\14\1\uffff\1\17\1\16",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "157:1: stmt : ( stmt_assign | stmt_func | stmt_yield | stmt_return | stmt_break | stmt_if | stmt_print | stmt_while | stmt_for | stmt_for_list | stmt_get | stmt_switch | stmt_coroutine | stmt_next );";
        }
    }
    static final String DFA20_eotS =
        "\4\uffff";
    static final String DFA20_eofS =
        "\4\uffff";
    static final String DFA20_minS =
        "\2\5\2\uffff";
    static final String DFA20_maxS =
        "\2\100\2\uffff";
    static final String DFA20_acceptS =
        "\2\uffff\1\2\1\1";
    static final String DFA20_specialS =
        "\4\uffff}>";
    static final String[] DFA20_transitionS = {
            "\1\2\1\1\1\3\50\uffff\1\3\1\uffff\2\3\4\uffff\1\3\1\uffff\3"+
            "\3\1\uffff\3\3",
            "\1\2\1\1\1\3\50\uffff\1\3\1\uffff\2\3\4\uffff\1\3\1\uffff"+
            "\3\3\1\uffff\3\3",
            "",
            ""
    };

    static final short[] DFA20_eot = DFA.unpackEncodedString(DFA20_eotS);
    static final short[] DFA20_eof = DFA.unpackEncodedString(DFA20_eofS);
    static final char[] DFA20_min = DFA.unpackEncodedStringToUnsignedChars(DFA20_minS);
    static final char[] DFA20_max = DFA.unpackEncodedStringToUnsignedChars(DFA20_maxS);
    static final short[] DFA20_accept = DFA.unpackEncodedString(DFA20_acceptS);
    static final short[] DFA20_special = DFA.unpackEncodedString(DFA20_specialS);
    static final short[][] DFA20_transition;

    static {
        int numStates = DFA20_transitionS.length;
        DFA20_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA20_transition[i] = DFA.unpackEncodedString(DFA20_transitionS[i]);
        }
    }

    class DFA20 extends DFA {

        public DFA20(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 20;
            this.eot = DFA20_eot;
            this.eof = DFA20_eof;
            this.min = DFA20_min;
            this.max = DFA20_max;
            this.accept = DFA20_accept;
            this.special = DFA20_special;
            this.transition = DFA20_transition;
        }
        public String getDescription() {
            return "()* loopback of 406:7: ( ( NEWLINE )* stmt )*";
        }
    }
 

    public static final BitSet FOLLOW_functions_in_program110 = new BitSet(new long[]{0x0000800000000040L});
    public static final BitSet FOLLOW_NEWLINE_in_program112 = new BitSet(new long[]{0x0000800000000040L});
    public static final BitSet FOLLOW_main_in_program115 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_program117 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NEWLINE_in_functions161 = new BitSet(new long[]{0x0000100000000040L});
    public static final BitSet FOLLOW_function_in_functions164 = new BitSet(new long[]{0x0000100000000042L});
    public static final BitSet FOLLOW_44_in_function218 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_ID_in_function220 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_function233 = new BitSet(new long[]{0x0000400000000080L});
    public static final BitSet FOLLOW_params_in_function235 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_function237 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_block_in_function239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_param_in_params313 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_COMMA_in_params355 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_param_in_params361 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_ID_in_param410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_declarator427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_main459 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_main461 = new BitSet(new long[]{0x0000400000000080L});
    public static final BitSet FOLLOW_params_in_main463 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_main465 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_block_in_main467 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_main470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_assign_in_stmt510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_func_in_stmt527 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_yield_in_stmt544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_return_in_stmt560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_break_in_stmt576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_if_in_stmt592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_print_in_stmt609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_while_in_stmt626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_for_in_stmt643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_for_list_in_stmt659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_get_in_stmt676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_switch_in_stmt692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_coroutine_in_stmt707 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stmt_next_in_stmt722 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_stmt_switch759 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_stmt_switch761 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_stmt_switch763 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_stmt_switch765 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_INDENT_in_stmt_switch767 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_case_stmt_in_stmt_switch770 = new BitSet(new long[]{0x0002000000000020L,0x0000000000000001L});
    public static final BitSet FOLLOW_stmt_break_in_stmt_switch774 = new BitSet(new long[]{0x0002000000000020L});
    public static final BitSet FOLLOW_DEDENT_in_stmt_switch781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_case_stmt814 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_INT_in_case_stmt816 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_block_in_case_stmt818 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_stmt_get870 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_ID_in_stmt_get872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_stmt_print921 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_stmt_print923 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_stmt_assign968 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ID_in_stmt_assign987 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_52_in_stmt_assign989 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_stmt_assign991 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_53_in_stmt_assign993 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ASSIGN_in_stmt_assign1008 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_value_in_stmt_assign1010 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_value1057 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_list_in_value1078 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_list1109 = new BitSet(new long[]{0x0030200000100180L});
    public static final BitSet FOLLOW_expr_in_list1117 = new BitSet(new long[]{0x0020000010000000L});
    public static final BitSet FOLLOW_COMMA_in_list1133 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_list1140 = new BitSet(new long[]{0x0020000010000000L});
    public static final BitSet FOLLOW_53_in_list1160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_stmt_next1196 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_54_in_stmt_next1198 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_stmt_coroutine1231 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_55_in_stmt_coroutine1233 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_stmt_coroutine1235 = new BitSet(new long[]{0x0010600000100180L});
    public static final BitSet FOLLOW_call_params_in_stmt_coroutine1237 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_stmt_coroutine1239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_stmt_func1297 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_stmt_func1299 = new BitSet(new long[]{0x0010600000100180L});
    public static final BitSet FOLLOW_call_params_in_stmt_func1301 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_stmt_func1303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_stmt_if1358 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_stmt_if1360 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_log_stmt_in_stmt_if1362 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_stmt_if1364 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_block_in_stmt_if1366 = new BitSet(new long[]{0x0200000000000002L});
    public static final BitSet FOLLOW_stmt_else_in_stmt_if1369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_57_in_stmt_else1414 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_block_in_stmt_else1416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_58_in_stmt_while1448 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_stmt_while1450 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_log_stmt_in_stmt_while1452 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_stmt_while1454 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_block_in_stmt_while1456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_stmt_for1510 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_stmt_for1512 = new BitSet(new long[]{0x0000000000000480L});
    public static final BitSet FOLLOW_for_stmts_in_stmt_for1514 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_stmt_for1516 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_block_in_stmt_for1518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_for_assign_in_for_stmts1562 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_SEMI_in_for_stmts1566 = new BitSet(new long[]{0x0010200000100580L});
    public static final BitSet FOLLOW_log_stmt_in_for_stmts1568 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_SEMI_in_for_stmts1571 = new BitSet(new long[]{0x0000000000000082L});
    public static final BitSet FOLLOW_for_assign_in_for_stmts1577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_for_assign1634 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ASSIGN_in_for_assign1649 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_value_in_for_assign1651 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_stmt_for_list1700 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_stmt_for_list1702 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_ID_in_stmt_for_list1708 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_61_in_stmt_for_list1712 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_stmt_for_list1718 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_stmt_for_list1720 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_block_in_stmt_for_list1722 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_stmt_yield1772 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_stmt_yield1774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_63_in_stmt_return1820 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_stmt_return1822 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_stmt_break1868 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_call_params1905 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_COMMA_in_call_params1909 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_call_params1913 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_NEWLINE_in_stmts1943 = new BitSet(new long[]{0xDD1D2000001001C0L,0x0000000000000001L});
    public static final BitSet FOLLOW_stmt_in_stmts1946 = new BitSet(new long[]{0xDD1D2000001001C2L,0x0000000000000001L});
    public static final BitSet FOLLOW_expr_in_log_stmt1977 = new BitSet(new long[]{0x0000000400000800L});
    public static final BitSet FOLLOW_LOGOP_in_log_stmt1981 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_log_stmt1987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_is_expr_in_log_stmt2034 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOT_in_is_expr2080 = new BitSet(new long[]{0x0000000000007000L});
    public static final BitSet FOLLOW_ISINT_in_is_expr2083 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ISSTRING_in_is_expr2102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ISLIST_in_is_expr2119 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_mult_expr_in_expr2160 = new BitSet(new long[]{0x0000000060000002L});
    public static final BitSet FOLLOW_PLUS_in_expr2164 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_MINUS_in_expr2168 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_mult_expr_in_expr2176 = new BitSet(new long[]{0x0000000060000002L});
    public static final BitSet FOLLOW_atom_in_mult_expr2242 = new BitSet(new long[]{0x0000000180000002L});
    public static final BitSet FOLLOW_STAR_in_mult_expr2251 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_SLASH_in_mult_expr2255 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_atom_in_mult_expr2264 = new BitSet(new long[]{0x0000000180000002L});
    public static final BitSet FOLLOW_ID_in_atom2326 = new BitSet(new long[]{0x0010000400000002L});
    public static final BitSet FOLLOW_stmt_func_in_atom2342 = new BitSet(new long[]{0x0010000400000002L});
    public static final BitSet FOLLOW_element_in_atom2358 = new BitSet(new long[]{0x0010000400000002L});
    public static final BitSet FOLLOW_list_in_atom2374 = new BitSet(new long[]{0x0010000400000002L});
    public static final BitSet FOLLOW_45_in_atom2389 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_atom2391 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_atom2393 = new BitSet(new long[]{0x0010000400000002L});
    public static final BitSet FOLLOW_stmt_next_in_atom2408 = new BitSet(new long[]{0x0010000400000002L});
    public static final BitSet FOLLOW_index_in_atom2428 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_point_expr_in_atom2436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOT_in_point_expr2487 = new BitSet(new long[]{0x00000000000F8000L});
    public static final BitSet FOLLOW_TOINT_in_point_expr2490 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TOSTRING_in_point_expr2512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LENGTH_in_point_expr2533 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_list_ops_in_point_expr2554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DEL_in_list_ops2583 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_list_ops2585 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_list_ops2587 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_list_ops2589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ADD_in_list_ops2603 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_list_ops2605 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_list_ops2607 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_list_ops2609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_index2637 = new BitSet(new long[]{0x0010200000100180L});
    public static final BitSet FOLLOW_expr_in_index2639 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_53_in_index2641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INDENT_in_block2712 = new BitSet(new long[]{0xDD1D2000001001E0L,0x0000000000000001L});
    public static final BitSet FOLLOW_stmts_in_block2714 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_NEWLINE_in_block2716 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_DEDENT_in_block2719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_element0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_synpred22_SimpleLists1057 = new BitSet(new long[]{0x0000000000000002L});

}