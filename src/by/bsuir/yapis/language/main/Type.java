package by.bsuir.yapis.language.main;

public class Type {

	private Type varType;
	
	public Type() {		
	}
	
	public Type(int value) {
		setVarType(new Element(String.valueOf(value), ElType.INTEGER));
	}
	public Type(String value) {
		setVarType(new Element(value, ElType.STRING));
	}
	
	public Type(Iterable iter) {
		setVarType(new SimpleList(iter));			
	}
	
	public Type(Object[] array) {
		setVarType(new SimpleList(array));			
	}
	
	public Type(Type copy) {
		Type toCopy = copy.getVarType();
		if (toCopy.getClass().getSimpleName().equals("Element"))
			varType = new Element((Element)toCopy);
		else varType = new SimpleList((SimpleList)toCopy);
	}

	public Type length() {
		return ((SimpleList)this.getVarType()).length();
	}
	
	public void set(Type value) {
		if(value.getVarType().getClass().getSimpleName().equals("Element"))
			varType = new Element((Element)value.getVarType());
		else varType = new SimpleList((SimpleList)value.getVarType());
	}
	
	public void set(int value) {
		((Element)varType).set(value);
	}
	
	public Type get(Type value) {
		return ((SimpleList)varType).getElements().get(Integer.parseInt(((Element)value.getVarType()).getValue()));
	}

	public Type getVarType() {
		return varType;
	}

	public void setVarType(Type varType) {
		this.varType = varType;
	}
	
	public Type changeType(ElType type) {
		if (this.varType instanceof SimpleList)
			return new Type(this);
		else return ((Element)this.varType).changeType(type);
	}
	
	public boolean less(Type comp) {
		return varType.less(comp.getVarType());
	}
	
	public boolean greater(Type comp) {
		return varType.greater(comp.getVarType());
	}

	public boolean less(int comp) {
		return less(new Type(comp));
	}
	
	public boolean greater(int comp) {
		return greater(new Type(String.valueOf(comp)));
	}
	
	public Type add(int i) {
		return varType.add(i);
	}
	public Type add(String s) {
		return varType.add(s);
	}
	public Type add(SimpleList l){
		return null;
	}
	public Type add(Object[] arr) {
		return varType.add(new SimpleList(arr));
	}
	public Type add(Type arr) {
		if (arr.getType() == ElType.LIST)
			return varType.add((SimpleList)arr.getVarType());
		else if (getType() == ElType.LIST)
			return ((SimpleList)varType).add(arr);
		else return ((Element)varType).add((Element)arr.getVarType());
	}
	
	public Type sub(int i) {
		return varType.sub(i);
	}
	public Type sub(String s) {
		return varType.sub(s);
	}
	public Type sub(Element l){
		return null;
	}
	public Type sub(SimpleList l){
		return null;
	}
	public Type sub(Object[] arr) {
		return varType.sub(new SimpleList(arr));
	}
	
	public Type sub(Type t) {
		Type toSub = t.getVarType();
		if(toSub.getClass().getSimpleName().equals("Element"))
			return varType.sub((Element)toSub);
		else return ((SimpleList)varType).sub((SimpleList)toSub);
	}
	 
	public Type mult(int t) {
		if (getType() == ElType.INTEGER)
			return (Element)varType.mult(t);
		else return new Type(this);
	}
	
	public Type div(int t) {
		if (getType() == ElType.INTEGER)
			return (Element)varType.div(t);
		else return new Type(this);
	}
	
	public Type mult(Type t) {
		if (getType() == ElType.LIST && t.getType() == ElType.LIST)
			return ((SimpleList)varType).mult((SimpleList)t.getVarType());
		else if (getType() == ElType.INTEGER && t.getType() == ElType.INTEGER)
			return ((Element)varType).mult((Element)t.getVarType());
		else return new Type(this);
	}
	
	public Type div(Type t) {
		if (getType() == ElType.LIST && t.getType() == ElType.LIST)
			return ((SimpleList)varType).symSub((SimpleList)t.getVarType());
		else if (getType() == ElType.INTEGER && t.getType() == ElType.INTEGER)
			return ((Element)varType).div((Element)t.getVarType());
		else return new Type(this);			
	}
	
	public Type del(Type t) {
		if (getType() != ElType.LIST ||	t.getType() != ElType.INTEGER)
			return new Type(this);
		else return ((SimpleList)varType).del(Integer.parseInt(((Element)t.getVarType()).getValue()));			
	}	
	public Type del(int i) {
		if (getType() != ElType.LIST)
			return new Type(this);
		else return ((SimpleList)varType).del(i);			
	}
	
	public ElType getType() {
		if (varType instanceof SimpleList)
			return ElType.LIST;
		else return varType.getType();
	}
	
	public boolean notEquals(Type comp) {
		return !equals(comp);
	}
	
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof Type))
			return false;
		
		Type comp = (Type)obj;
		if(varType.getClass() != comp.getVarType().getClass())
			return false;
		
		return varType.equals(comp.getVarType());
	}
	
	public Type addEl(Type toAdd) {
		if (varType instanceof SimpleList)
			return varType.add(toAdd);
		else return add(toAdd);
	}
	
	@Override
	public int hashCode() {
		int code = 11;
		code = code*7 + varType.hashCode();
		return code;
	}
	
	@Override
	public String toString(){
		if (varType.getClass().getSimpleName().equals("Element"))
			return ((Element)varType).toString();
		else return ((SimpleList)varType).toString();
	}
	
}
