
import java.util.Scanner;
import by.bsuir.yapis.language.main.*;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;
import jyield.Continuable;
import jyield.Yield;

public class program
{
    private static Scanner scan = new Scanner(System.in);
    private static String toScan;
    private static Iterator corout;
    
    public static Type swapTypes(Type ls) {
        Type i = new Type((new Type(3)));
        for(i.set((new Type(0))); i.less(ls.length()); i.set(i.add((new Type(1))))) {
            if(ls.get(i).getType() == ElType.INTEGER) {
                for(Type j = new Type(i.add((new Type(1)))); j.less(ls.length()); j.set(j.add((new Type(1))))) {
                    if(ls.get(j).changeType(ElType.INTEGER).equals(ls.get(i))) {
                        ls.get(i).set(ls.get(i).changeType(ElType.STRING));
                        ls.get(j).set(ls.get(j).changeType(ElType.INTEGER));
                        break;
                     }
                        ;
                };
             }
             else {
                 for(Type j = new Type(i.add((new Type(1)))); j.less(ls.length()); j.set(j.add((new Type(1))))) {
                     if(ls.get(j).changeType(ElType.STRING).equals(ls.get(i))) {
                         ls.get(i).set(ls.get(i).changeType(ElType.INTEGER));
                         ls.get(j).set(ls.get(j).changeType(ElType.STRING));
                         break;
                      }
                         ;
                 };
             }    ;
        };
        return new Type(ls);
    }
    public static Type bubbleSort(Type lst) {
        for(Type i = new Type((new Type(0))); i.less(lst.length()); i.set(i.add((new Type(1))))) {
            Type min = new Type(i);
            for(Type j = new Type(i.add((new Type(1)))); j.less(lst.length()); j.set(j.add((new Type(1))))) {
                if(lst.get(j).less(lst.get(min))) {
                    min.set(j);
                 }
                    ;
            };
            Type t = new Type(lst.get(i));
            lst.get(i).set(lst.get(min));
            lst.get(min).set(t);
        };
        for(Type inside_lst: ((SimpleList)lst.getVarType()).getElements()) {
            if(inside_lst.getType() == ElType.LIST) {
                inside_lst.set(bubbleSort(inside_lst));
             }
                ;
        };
        return new Type(lst);
    }
    public static Type quickSort(Type lst, Type low, Type high) {
        Type i = new Type(low);
        Type j = new Type(high);
        Type m = new Type(lst.get((i.add(j)).div((new Type(2)))));
        while(i.less(j)) {
            while(lst.get(i).less(m)) {
                i.set(i.add((new Type(1))));
            };
            while(lst.get(j).greater(m)) {
                j.set(j.sub((new Type(1))));
            };
            if(i.less(j.add((new Type(1))))) {
                Type temp = new Type(lst.get(i));
                lst.get(i).set(lst.get(j));
                lst.get(j).set(temp);
                i.set(i.add((new Type(1))));
                j.set(j.sub((new Type(1))));
             }
                ;
        };
        if(low.less(j)) {
            quickSort(lst,low,j);
         }
            ;
        if(i.less(high)) {
            quickSort(lst,i,high);
         }
            ;
        return new Type(lst);
    }
    public static Type removeAll(Type lst, Type toRemove) {
        for(Type remEl: ((SimpleList)toRemove.getVarType()).getElements()) {
            lst.set(lst.sub(remEl));
            for(Type internList: ((SimpleList)lst.getVarType()).getElements()) {
                if(internList.getType() == ElType.LIST) {
                    removeAll(internList,toRemove);
                 }
                    ;
            };
        };
        return new Type(lst);
    }
    @Continuable
    public static Iterable<Type> findData(Type ls) {
        for(Type el: ((SimpleList)ls.getVarType()).getElements()) {
            if(el.getType() == ElType.INTEGER) {
                Yield.ret(new Type(el));;
             }
                ;
        };
        return Yield.done();
    }
    @Continuable
    public static Iterable<Type> hello() {
        System.out.print((new Type("goodbye, ")).toString());
        Yield.ret(new Type((new Type(0))));;
        System.out.print((new Type("cruel ")).toString());
        Yield.ret(new Type((new Type(0))));;
        System.out.print((new Type("world")).toString());
        return Yield.done();
    }
    
    public static void main(String args[]) {
        Type a = new Type(new Type(new Object[]{(new Type("5")),new Type(new Object[]{(new Type(3)),(new Type(2)),(new Type(1))}),(new Type(3)),(new Type("3")),(new Type(5))}));
        Type ls = new Type(new Type(new Object[]{(new Type(1)),(new Type(2)),(new Type(1)),(new Type(2)),(new Type(2)),(new Type(2)),(new Type(1))}));
        for(Type el: ((SimpleList)(new Type(findData(ls))).getVarType()).getElements()) {
            switch(Integer.parseInt(((Element)(el).getVarType()).getValue())) {
                    case 1:
                    {
                        System.out.print((new Type("one ")).toString());
                        break;
                    }
                    case 2:
                    {
                        System.out.print((new Type("two ")).toString());
                    }
            };
        };
    }
}
